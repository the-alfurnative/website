import { createIndexRoute } from "../Utilities/Routing/createIndexRoute";
import { CustomRouteObject } from "../Utilities";

import { LegalPage } from "./LegalPage";
import { RelaxedDresscode } from "./RelaxedDresscode";

import CodeOfConductIcon from "~icons/mdi/numeric-1";
import RelaxedDresscodeIcon from "~icons/mdi/numeric-2";

export const LegalRoutes = (): CustomRouteObject[] => [
    {
        path: "legal",
        name: "Legal",
        icon: <></>,
        element: <LegalPage />,
        children: [
            createIndexRoute("code-of-conduct"),
            {
                path: "code-of-conduct",
                name: "Code of Conduct",
                icon: <CodeOfConductIcon />,
                element: (
                    <iframe src={"/Code of Conduct.pdf"} height={"100%"} />
                ),
            },
            {
                path: "relaxed-dresscode",
                name: "Relaxed Dresscode",
                icon: <RelaxedDresscodeIcon />,
                element: <RelaxedDresscode />,
            },
        ] as CustomRouteObject[],
    },
];
