import {
    Box,
    Card,
    CardContent,
    Chip,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    ListSubheader,
    Stack,
    Typography,
    useTheme,
} from "@mui/material";

import { Logo } from "../Home/Logo";

import AllowedIcon from "~icons/mdi/check";
import BannedIcon from "~icons/mdi/close";

export const RelaxedDresscode = () => {
    const { palette } = useTheme();
    return (
        <Box
            display={"flex"}
            justifyContent={"space-around"}
            flexDirection={"row"}
            width={"100%"}
        >
            <Card
                sx={{
                    mt: 2,
                    maxWidth: 1020,
                    width: "100%",
                    overflowX: "auto",
                }}
            >
                <Stack spacing={2} flexWrap={"wrap"}>
                    <Stack
                        direction={"row"}
                        alignItems={"center"}
                        spacing={8}
                        flexWrap={"wrap"}
                    >
                        <Box width={400} maxWidth={"80%"}>
                            <Logo height={300} />
                        </Box>
                        <Typography
                            variant={"h2"}
                            textAlign={"center"}
                            flex={1}
                        >
                            AFn Relaxed Dresscode
                        </Typography>
                    </Stack>
                    <Stack
                        direction={"row"}
                        spacing={2}
                        justifyContent={"space-between"}
                    >
                        <List sx={{ flex: 1, minWidth: 300, maxWidth: "100%" }}>
                            <ListSubheader
                                disableSticky
                                sx={{ fontSize: "1.3rem" }}
                            >
                                Allowed at Relaxed Dresscode
                            </ListSubheader>
                            <ListItem>
                                <ListItemIcon>
                                    <AllowedIcon color={palette.success.main} />
                                </ListItemIcon>
                                <ListItemText
                                    primary={"Puphoods, Gasmasks and similiar"}
                                />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <AllowedIcon color={palette.success.main} />
                                </ListItemIcon>
                                <ListItemText
                                    primary={
                                        "Petsuits, Leather, Latex, Harnesses"
                                    }
                                    secondary={
                                        <Stack
                                            spacing={1}
                                            direction={"row"}
                                            pt={1}
                                            flexWrap={"wrap"}
                                        >
                                            <Chip
                                                color={"warning"}
                                                label={"Clothing Required"}
                                            />
                                            <Chip
                                                color={"warning"}
                                                label={"Crotch covered"}
                                            />
                                        </Stack>
                                    }
                                />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <AllowedIcon color={palette.success.main} />
                                </ListItemIcon>
                                <ListItemText
                                    primary={"Jockstraps"}
                                    secondary={
                                        <Stack
                                            spacing={1}
                                            direction={"row"}
                                            pt={1}
                                            flexWrap={"wrap"}
                                        >
                                            <Chip
                                                color={"warning"}
                                                label={"Over gear/fursuit"}
                                            />
                                            <Chip
                                                color={"warning"}
                                                label={"No naked buttocks"}
                                            />
                                        </Stack>
                                    }
                                />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <AllowedIcon color={palette.success.main} />
                                </ListItemIcon>
                                <ListItemText
                                    primary={"Cuffs, Fetish collars"}
                                />
                            </ListItem>
                        </List>
                        <List sx={{ flex: 1, minWidth: 300 }}>
                            <ListSubheader
                                disableSticky
                                sx={{ fontSize: "1.3rem" }}
                            >
                                Not allowed at Relaxed Dresscode
                            </ListSubheader>
                            <ListItem>
                                <ListItemIcon>
                                    <BannedIcon color={palette.error.main} />
                                </ListItemIcon>
                                <ListItemText
                                    primary={
                                        "ABDL Gear, NSFW Toys, Handheld Items"
                                    }
                                />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <BannedIcon color={palette.error.main} />
                                </ListItemIcon>
                                <ListItemText
                                    primary={"Exposed Nipples/Genitals"}
                                    secondary={
                                        <Stack
                                            spacing={1}
                                            direction={"row"}
                                            pt={1}
                                        >
                                            <Chip
                                                color={"warning"}
                                                label={"All Genders"}
                                            />
                                            <Chip
                                                color={"warning"}
                                                label={"Includes buttocks"}
                                            />
                                        </Stack>
                                    }
                                />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <BannedIcon color={palette.error.main} />
                                </ListItemIcon>
                                <ListItemText primary={"Leashes of any kind"} />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <BannedIcon color={palette.error.main} />
                                </ListItemIcon>
                                <ListItemText primary={"Open footwear"} />
                            </ListItem>
                        </List>
                    </Stack>
                    <CardContent>
                        <Typography variant={"h4"}>Reminder</Typography>
                        <ul>
                            <li>
                                This applies only to the relevant Event Space
                            </li>
                            <li>
                                You are not allowed to wear the gear around the
                                convention area
                            </li>
                            <li>
                                Cover yourself and your gear while walking to
                                and from the event
                            </li>
                            <li>
                                Don't be mostly naked, wear at least a few
                                pieces of clothing
                            </li>
                            <li>Sexual acts are strictly prohibited</li>
                            <li>
                                You have to wear closed footwear at all times
                            </li>
                        </ul>
                        <Typography>
                            For further questions see the Code of Conduct or
                            message @lightyheart (AFn Security Director) or
                            @AlpeKojot (AFn Medical Officer) on Telegram
                        </Typography>
                    </CardContent>
                </Stack>
            </Card>
        </Box>
    );
};
