import {
    Box,
    Divider,
    List,
    ListItemButton,
    ListItemIcon,
} from "@mui/material";
import { Link, Outlet, useMatch } from "react-router-dom";

import {
    SidebarListCollapseToggle,
    SidebarListItemText,
    useSidebarProps,
} from "../Layout";
import { ListItemRoutes } from "../Utilities";

import { LegalRoutes } from "./routes";

import TermsOfServiceIcon from "~icons/mdi/scale-balance";

export const LegalPage = () => {
    const match = useMatch("/legal/:element");
    const { containerSx, sidebarSx, contentSx, open, toggle } =
        useSidebarProps();

    return (
        <Box sx={containerSx}>
            <List sx={sidebarSx}>
                <SidebarListCollapseToggle open={open} toggle={toggle} />
                <ListItemRoutes routes={LegalRoutes()} open={open} />

                <ListItemButton
                    component={Link}
                    rel={"noopener"}
                    target={"_blank"}
                    to={
                        "https://www.evernote.com/shard/s719/sh/fb16c279-f1ea-56f9-69a5-c4c683f466bb/605ef46c7eab6e82650b13e269bc4925"
                    }
                    selected={match?.params.element === "terms-of-service"}
                >
                    <ListItemIcon>
                        <TermsOfServiceIcon />
                    </ListItemIcon>
                    <SidebarListItemText
                        open={open}
                        primary={"Terms of Service"}
                    />
                </ListItemButton>
            </List>
            <Divider orientation={"vertical"} />
            <Box sx={contentSx}>
                <Outlet />
            </Box>
        </Box>
    );
};
