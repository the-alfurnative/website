import { useQuery } from "react-query";
import { Container, Divider, LinearProgress, Typography } from "@mui/material";

import { backendKeys, getPublicInformation } from "../../../apis";

import { FAQViewer } from "./FAQViewer";

export const FAQSection = () => {
    const {
        data: { questions } = { questions: [] },
        isFetching,
        isSuccess,
    } = useQuery(backendKeys.publicInformation(), () => getPublicInformation());

    if (isSuccess && questions.length === 0) {
        return null;
    }
    return (
        <Container sx={{ mt: 4 }}>
            <Typography variant={"h3"} gutterBottom>
                Frequently Asked Questions
            </Typography>
            <Divider sx={{ my: 2 }} />
            {isFetching ? (
                <LinearProgress />
            ) : (
                <FAQViewer questions={questions} />
            )}
        </Container>
    );
};
