import { Remark } from "react-remark";
import { Card, CardContent, CardHeader } from "@mui/material";

import { PublicQuestion } from "../../../apis";
import { Brick, Masonry } from "../../Layout/Masonry";

export const FAQViewer = ({ questions }: FAQViewerProps) => {
    return (
        <Masonry columnCount={2}>
            {questions.map((it) => (
                <Brick>
                    <Card>
                        <CardHeader
                            title={it.question}
                            subheader={it.category}
                        />
                        <CardContent sx={{ overflowX: "auto" }}>
                            <Remark>{it.answer}</Remark>
                        </CardContent>
                    </Card>
                </Brick>
            ))}
        </Masonry>
    );
};
export type FAQViewerProps = {
    questions: PublicQuestion[];
};
