import { Card, CardContent, CardHeader, Container } from "@mui/material";
import { Remark } from "react-remark";

import message from "../../message.md?raw";

export const HomePage = () => {
    return (
        <>
            <Container sx={{ pt: 4 }}>
                <Card>
                    <CardHeader
                        title={
                            "This message is being posted on behalf of the chairman of Alfurnative, Hunter."
                        }
                        subheader={
                            "It's with pain in my heart that I have to give you this update."
                        }
                    />
                    <CardContent>
                        <Remark>{message}</Remark>
                    </CardContent>
                </Card>
            </Container>
        </>
    );
};
