import "./LocationCard.css";

import { Marker, Popup } from "react-leaflet";
import { Stack, Typography } from "@mui/material";
import L from "leaflet";
import { useQuery } from "react-query";
import { Remark } from "react-remark";
import { match } from "ts-pattern";

import { LeafletMap } from "../Utilities/Leaflet/LeafletMap";
import { backendKeys, getPublicInformation } from "../../apis";

import HotelIcon from "~icons/material-symbols/hotel?raw";
import ParkingIcon from "~icons/material-symbols/local-parking?raw";
import DrinkIcon from "~icons/ep/cold-drink?raw";
import RestaurantIcon from "~icons/mdi/silverware?raw";

const hotelIcon = L.divIcon({
    html: HotelIcon as unknown as string,
    iconSize: [42, 42],
    className: "leaflet-marker-venue",
});

const drinkIcon = L.divIcon({
    html: DrinkIcon as unknown as string,
    iconSize: [42, 42],
    className: "leaflet-marker-location",
});

const parkingIcon = L.divIcon({
    html: ParkingIcon as unknown as string,
    iconSize: [42, 42],
    className: "leaflet-marker-parking",
});
const restaurantIcon = L.divIcon({
    html: RestaurantIcon as unknown as string,
    iconSize: [42, 42],
    className: "leaflet-marker-location",
});

export const LocationCard = () => {
    const { data } = useQuery(backendKeys.publicInformation(), () =>
        getPublicInformation()
    );

    return (
        <LeafletMap
            sx={{
                height: 550,
                maxWidth: "100vw",
            }}
        >
            {data?.locations?.map((it) => (
                <Marker
                    key={it.id}
                    position={[it.coordinate_lat, it.coordinate_lon]}
                    icon={match(it.category)
                        .with("parking", () => parkingIcon)
                        .with("hotel", () => hotelIcon)
                        .with("location", "bar", () => drinkIcon)
                        .with("restaurant", () => restaurantIcon)
                        .exhaustive()}
                >
                    <Popup>
                        <Stack spacing={2} sx={{ width: 400, maxWidth: 400 }}>
                            <Typography variant={"h6"}>{it.title}</Typography>
                            <Remark>{it.description}</Remark>
                        </Stack>
                    </Popup>
                </Marker>
            ))}
        </LeafletMap>
    );
};
