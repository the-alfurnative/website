import { Box, Typography, useTheme } from "@mui/material";

import { Page } from "../Layout/Page";

import CheckIcon from "~icons/mdi/check";

export const AuthenticationSuccess = () => {
    const theme = useTheme();
    return (
        <Page>
            <Box
                display={"flex"}
                alignItems={"center"}
                justifyContent={"center"}
                flexDirection={"column"}
                flex={1}
                gap={5}
            >
                <Typography variant={"h3"}>You have been logged in!</Typography>
                <CheckIcon
                    height={42}
                    width={42}
                    color={theme.palette.success.main}
                />
            </Box>
        </Page>
    );
};
