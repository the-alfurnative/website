import { useMemo } from "react";
import { useOidcUser } from "@axa-fr/react-oidc";
import type { OidcUserInfo } from "@axa-fr/react-oidc";
export const useRoles = (): string[] | undefined => {
    const user = useOidcUser<
        { "https://alfurnative.nl/roles": string[] } & OidcUserInfo
    >();

    return useMemo(() => {
        if (user.oidcUser === undefined || user.oidcUser === null) {
            return undefined;
        }

        return user.oidcUser["https://alfurnative.nl/roles"];
    }, [user]);
};
