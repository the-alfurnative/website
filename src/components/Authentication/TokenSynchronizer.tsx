import { useOidcIdToken } from "@axa-fr/react-oidc";
import { useEffect } from "react";

import { setFetchToken } from "../../utilities/axios";

/**
 * Syncs the token from the react state to the global state.
 * @constructor
 */
export const TokenSynchronizer = () => {
    const { idToken: accessToken } = useOidcIdToken();

    useEffect(() => {
        setFetchToken(accessToken);
    }, [accessToken]);

    return null;
};
