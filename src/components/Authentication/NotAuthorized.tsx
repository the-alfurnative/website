import {
    Card,
    CardHeader,
    Grid,
    IconButton,
    LinearProgress,
} from "@mui/material";
import { useOidc } from "@axa-fr/react-oidc";

import { Page } from "../Layout/Page";

import LoginIcon from "~icons/mdi/account";

export const NotAuthorized = () => {
    const { login } = useOidc();
    return (
        <Page>
            <Grid
                container
                spacing={2}
                justifyContent={"center"}
                alignContent={"center"}
            >
                <Grid item xs={12} md={6} lg={4}>
                    <Card sx={{ mt: 8 }}>
                        <CardHeader
                            title={"You are not authenticated"}
                            subheader={"Wait for authentication to complete"}
                            action={
                                <IconButton onClick={() => login()}>
                                    <LoginIcon />
                                </IconButton>
                            }
                        />
                        <LinearProgress />
                    </Card>
                </Grid>
            </Grid>
        </Page>
    );
};
