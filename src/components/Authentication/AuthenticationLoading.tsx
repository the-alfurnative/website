import { Box, CircularProgress, Typography } from "@mui/material";

import { Page } from "../Layout";

export const AuthenticationLoading = () => (
    <Page>
        <Box
            display={"flex"}
            alignItems={"center"}
            justifyContent={"center"}
            flexDirection={"column"}
            gap={5}
            flex={1}
        >
            <Typography variant={"h3"}>
                You are being logged in . . .
            </Typography>
            <CircularProgress color={"info"} size={42} />
        </Box>
    </Page>
);
