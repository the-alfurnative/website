import {
    Box,
    Card,
    CardContent,
    CardHeader,
    Divider,
    IconButton,
    Typography,
} from "@mui/material";
import { useNavigate } from "react-router-dom";

import { Page } from "../Layout/Page";

import BackIcon from "~icons/mdi/arrow-left";
import WarningIcon from "~icons/mdi/alert";

export const AuthorizationFailed = () => {
    const history = useNavigate();
    return (
        <Page>
            <Box
                display={"flex"}
                alignItems={"center"}
                justifyContent={"center"}
                flexDirection={"column"}
                flex={1}
                gap={5}
            >
                <Card sx={{ width: 450 }}>
                    <CardHeader
                        title={"Authorization failed"}
                        avatar={<WarningIcon />}
                        action={
                            <IconButton
                                onClick={() => history(-1)}
                                title={"Go back"}
                            >
                                <BackIcon />
                            </IconButton>
                        }
                    />
                    <Divider />
                    <CardContent>
                        <Typography>
                            You do not have the required access for this page!
                        </Typography>
                    </CardContent>
                </Card>
            </Box>
        </Page>
    );
};
