import { PropsWithChildren } from "react";
import type { OidcConfiguration } from "@axa-fr/react-oidc";
import { OidcProvider, OidcSecure } from "@axa-fr/react-oidc";

import { Page } from "../Layout";

import { TokenSynchronizer } from "./TokenSynchronizer";
import { AuthenticationLoading } from "./AuthenticationLoading";
import { AuthenticationSuccess } from "./AuthenticationSuccess";

const configuration: OidcConfiguration = {
    client_id: "nUmsPXggD1OiSwAACOtyxTpnhix9RmWe",
    redirect_uri: window.location.origin + "/callback",
    scope: "openid profile email picture nickname offline_access",
    authority: "https://the-alfurnative.eu.auth0.com",
    storage: localStorage,
};

export const CustomAuthProvider = ({ children }: PropsWithChildren) => {
    return (
        <OidcProvider
            configuration={configuration}
            authenticatingComponent={AuthenticationLoading}
            callbackSuccessComponent={AuthenticationSuccess}
            loadingComponent={Page}
            authenticatingErrorComponent={OidcSecure}
            sessionLostComponent={OidcSecure}
        >
            <TokenSynchronizer />
            {children}
        </OidcProvider>
    );
};
