import { PropsWithChildren } from "react";
import { OidcSecure } from "@axa-fr/react-oidc";
import { intersection } from "lodash-es";

import { useRoles } from "./useRoles";
import { AuthorizationFailed } from "./AuthorizationFailed";

export const InnerRolesRequired = ({
    roles,
    children,
}: PropsWithChildren<RolesRequiredProps>) => {
    const userRoles = useRoles();

    if (userRoles === undefined) {
        return null;
    }

    if (intersection(roles, userRoles).length === 0) {
        return <AuthorizationFailed />;
    }

    return <>{children}</>;
};

export const RolesRequired = ({
    roles,
    children,
}: PropsWithChildren<RolesRequiredProps>) => {
    return (
        <OidcSecure>
            <InnerRolesRequired roles={roles} children={children} />
        </OidcSecure>
    );
};

export type RolesRequiredProps = {
    roles: string[];
};
