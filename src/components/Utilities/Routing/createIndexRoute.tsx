import { IndexRouteObject, Navigate } from "react-router-dom";

export const createIndexRoute = (path: string): any =>
    ({
        index: true,
        path: "*",
        element: <Navigate to={path} replace />,
    } as IndexRouteObject);
