import { ListItemButton, ListItemIcon, ListItemText } from "@mui/material";
import { Link, useLocation } from "react-router-dom";
import { match, P } from "ts-pattern";
import { isArray } from "lodash-es";

import { SidebarSubheader } from "../../Layout/SidebarContent";

import { CustomRouteObject } from "./types";

export const ListItemRoute = ({
    route,
    open,
    currentMatch,
}: ListItemRouteProps) => {
    const location = useLocation();

    return match(route)
        .with({ stopIndex: true, name: P.string, path: P.string }, (res) => {
            return (
                <ListItemButton
                    component={Link as any}
                    to={res.path}
                    selected={location.pathname.includes(res.path)}
                >
                    <ListItemIcon>{route.icon}</ListItemIcon>
                    {open && <ListItemText primary={res.name} />}
                </ListItemButton>
            );
        })
        .with(
            {
                name: P.string,
                stopIndex: false,
                children: P.when(
                    (it): it is CustomRouteObject[] =>
                        isArray(it) && it.length > 0
                ),
            },
            {
                name: P.string,
                children: P.when(
                    (it): it is CustomRouteObject[] =>
                        isArray(it) && it.length > 0
                ),
            },
            (res) => (
                <>
                    <SidebarSubheader open={open}>{res.name}</SidebarSubheader>
                    {res.children.map((it) => (
                        <ListItemRoute
                            key={it.path}
                            route={it}
                            open={open}
                            currentMatch={currentMatch}
                        />
                    ))}
                </>
            )
        )
        .with(
            {
                path: P.string,
                name: P.string,
            },
            (res) => (
                <ListItemButton
                    component={Link as any}
                    to={res.path}
                    selected={location.pathname.includes(res.path)}
                >
                    <ListItemIcon>{route.icon}</ListItemIcon>
                    {open && <ListItemText primary={res.name} />}
                </ListItemButton>
            )
        )
        .with(
            {
                path: P.string,
                name: P.string,
            },
            (res) => (
                <ListItemButton
                    component={Link as any}
                    to={res.path}
                    selected={location.pathname.includes(res.path)}
                >
                    <ListItemIcon>{route.icon}</ListItemIcon>
                    {open && <ListItemText primary={res.name} />}
                </ListItemButton>
            )
        )
        .otherwise(() => null);
};
export type ListItemRouteProps = {
    route: CustomRouteObject;
    open: boolean;
    currentMatch?: string | undefined;
};

export const ListItemRoutes = ({
    routes,
    open,
    currentMatch,
}: ListItemRoutesProps) =>
    routes.map((it) => (
        <ListItemRoute route={it} open={open} currentMatch={currentMatch} />
    ));

export type ListItemRoutesProps = {
    routes: CustomRouteObject[];
    open: boolean;
    currentMatch?: string | undefined;
};
