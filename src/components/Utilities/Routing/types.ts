import { NonIndexRouteObject } from "react-router-dom";
import { ReactElement } from "react";

export type CustomRouteObject = Omit<NonIndexRouteObject, "children"> & {
    name?: string;
    icon?: ReactElement;
    stopIndex?: boolean;
    children?: CustomRouteObject[];
};
