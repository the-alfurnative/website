/* eslint-disable sort-exports/sort-exports */
import type {
    GridRenderEditCellParams,
    GridValidRowModel,
} from "@mui/x-data-grid-pro";
import { useGridApiContext } from "@mui/x-data-grid-pro";
import { MobileDateTimePicker } from "@mui/x-date-pickers";
import dayjs, { Dayjs } from "dayjs";
import type { GridColDef } from "@mui/x-data-grid";

export const GridDateTimeEditor = (props: GridRenderEditCellParams) => {
    const { id, value, field } = props;
    const apiRef = useGridApiContext();

    const handleChange = (newValue: Dayjs | null) => {
        apiRef.current.setEditCellValue({
            id,
            field,
            value: newValue?.toDate(),
        });
    };

    return (
        <MobileDateTimePicker<Dayjs>
            value={dayjs(value)}
            onAccept={handleChange}
        />
    );
};

export const DateTimeColumn: Pick<
    GridColDef<GridValidRowModel, Date, string | undefined>,
    "renderEditCell" | "valueFormatter"
> = {
    renderEditCell: GridDateTimeEditor,
    valueFormatter: ({ value }) =>
        value ? dayjs(value).format("lll") : undefined,
};
