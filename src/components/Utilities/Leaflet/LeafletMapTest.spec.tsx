import { describe, expect, it } from "vitest";
import { render, screen } from "@testing-library/react";

import { LeafletMap } from "./LeafletMap";

describe("<LeafletMap />", () => {
    describe("<LeafletMap />", () => {
        it("renders the leaflet map", async () => {
            render(<LeafletMap />);

            const result = await screen.findByTestId("LeafletMap");

            expect(result).toBeDefined();
        });
    });
});
