import "./LeafletMap.css";
import "leaflet/dist/leaflet.css";
import "leaflet/dist/leaflet";
import { PropsWithChildren } from "react";
import { MapContainer, MapContainerProps, TileLayer } from "react-leaflet";
import { Box, SxProps } from "@mui/material";

export const LeafletMap = ({
    children,
    sx,
    ...props
}: PropsWithChildren<MapContainerProps & { sx?: SxProps }>) => (
    <Box data-testid={"LeafletMap"} sx={sx}>
        <MapContainer
            center={[52.006934, 4.360638]}
            zoom={19}
            {...props}
            style={{ width: "100%", height: "100%" }}
        >
            <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {children}
        </MapContainer>
    </Box>
);
