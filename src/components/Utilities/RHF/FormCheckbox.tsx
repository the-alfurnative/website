import {
    Checkbox,
    CheckboxProps,
    FormControl,
    FormControlLabel,
    FormHelperText,
} from "@mui/material";
import { Controller, FieldValues } from "react-hook-form";
import { ReactNode } from "react";

import { InternalUseController } from "./common";

export const FormCheckbox = <T extends FieldValues = FieldValues>({
    name,
    label,
    ...checkboxProps
}: InternalUseController<T> &
    Omit<CheckboxProps, "inputRef" | "checked"> & { label: ReactNode }) => (
    <Controller<T>
        name={name}
        render={({ field, fieldState }) => (
            <FormControl error={fieldState.invalid}>
                <FormControlLabel
                    control={
                        <Checkbox
                            {...checkboxProps}
                            {...field}
                            inputRef={field.ref}
                            checked={field.value}
                        />
                    }
                    label={label}
                />
                {fieldState.error && (
                    <FormHelperText>{fieldState.error?.message}</FormHelperText>
                )}
            </FormControl>
        )}
    />
);
