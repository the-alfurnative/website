import { Box, TextField, TextFieldProps } from "@mui/material";
import { Remark } from "react-remark";
import { Controller, FieldValues } from "react-hook-form";

import { InternalUseController } from "./common";

export const FormMarkdownField = <T extends FieldValues = FieldValues>({
    name,
    helperText,
    rows,
    ...textFieldProps
}: Omit<TextFieldProps, "multiline" | "error" | "inputRef"> &
    InternalUseController<T>) => (
    <Controller
        name={name}
        render={({ field, fieldState }) => (
            <>
                <TextField
                    inputRef={field.ref}
                    {...field}
                    {...textFieldProps}
                    helperText={
                        fieldState.error ? fieldState.error.message : helperText
                    }
                    error={fieldState.invalid}
                    multiline
                    rows={rows ?? 8}
                />
                <Box
                    sx={(theme) => ({
                        flexGrow: 1,
                        flexShrink: 0,
                        minWidth: 0,
                        borderLeftColor: theme.palette.divider,
                        borderLeftWidth: 5,
                        borderLeftStyle: "solid",
                        pl: 4,
                        maxWidth: "100%",
                    })}
                >
                    <Remark>{field.value}</Remark>
                </Box>
            </>
        )}
    />
);
