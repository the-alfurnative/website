import { TextField, TextFieldProps } from "@mui/material";
import { Controller, FieldValues } from "react-hook-form";

import { InternalUseController } from "./common";

export const FormTextField = <T extends FieldValues = FieldValues>({
    name,
    helperText,
    ...textFieldProps
}: Omit<TextFieldProps, "error" | "inputRef"> & InternalUseController<T>) => (
    <Controller
        name={name}
        render={({ field, fieldState }) => (
            <TextField
                inputRef={field.ref}
                {...textFieldProps}
                {...field}
                error={fieldState.invalid}
                helperText={fieldState.error?.message ?? helperText}
            />
        )}
    />
);
