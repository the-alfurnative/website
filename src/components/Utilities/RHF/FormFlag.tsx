import { Controller, FieldValues } from "react-hook-form";
import { Checkbox, FormControlLabel } from "@mui/material";
import { concat, includes, remove, uniq } from "lodash-es";
import { ReactNode } from "react";

import { InternalUseController } from "./common";

export const FormFlag = <T extends FieldValues = FieldValues>({
    name,
    flag,
    label,
}: InternalUseController<T> & { flag: string; label?: ReactNode }) => (
    <Controller
        name={name}
        render={({ field, fieldState }) => {
            const flagInField = includes(field.value, flag);
            return (
                <FormControlLabel
                    control={
                        <Checkbox
                            {...field}
                            checked={flagInField}
                            inputRef={field.ref}
                            onChange={() =>
                                field.onChange(
                                    uniq(
                                        flagInField
                                            ? remove(field.value, flag)
                                            : concat(field.value, flag)
                                    )
                                )
                            }
                        />
                    }
                    label={
                        fieldState.invalid ? fieldState.error?.message : label
                    }
                />
            );
        }}
    />
);
