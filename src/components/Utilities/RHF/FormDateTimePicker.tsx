import { DateTimePicker, DateTimePickerProps } from "@mui/x-date-pickers";
import dayjs, { Dayjs } from "dayjs";
import { Controller, FieldValues } from "react-hook-form";

import { InternalUseController } from "./common";

export const FormDateTimePicker = <T extends FieldValues = FieldValues>({
    name,
    ...dateTimePickerProps
}: InternalUseController<T> & DateTimePickerProps<Dayjs>) => (
    <Controller
        name={name}
        render={({ field, fieldState }) => (
            <DateTimePicker<Dayjs>
                format={"LLLL"}
                {...dateTimePickerProps}
                {...field}
                value={field.value ? dayjs(field.value) : null}
                inputRef={field.ref}
                timezone={"Europe/Amsterdam"}
                slotProps={{
                    textField: {
                        ...dateTimePickerProps.slotProps?.textField,
                        error: fieldState.invalid,
                        helperText:
                            fieldState.error?.message ??
                            "A date and time. It is set to the Europe/Amsterdam timezone",
                    },
                }}
            />
        )}
    />
);
