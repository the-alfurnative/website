import { FieldValues, UseControllerProps } from "react-hook-form";

export type InternalUseController<T extends FieldValues> = Pick<
    UseControllerProps<T>,
    "name" | "rules"
>;
