import {
    Autocomplete,
    AutocompleteProps,
    TextField,
    TextFieldProps,
} from "@mui/material";
import { FieldValues, Controller } from "react-hook-form";

import { InternalUseController } from "./common";

export const FormAutocomplete = <
    FormType extends FieldValues = FieldValues,
    FieldType extends string | Record<string, any> = string
>({
    name,
    label,
    helperText,
    variant,
    ...autocompleteProps
}: InternalUseController<FormType> &
    Omit<
        AutocompleteProps<FieldType, boolean, boolean, boolean>,
        "renderInput"
    > &
    Pick<TextFieldProps, "label" | "helperText" | "variant">) => {
    return (
        <Controller
            name={name}
            render={({ field, fieldState }) => (
                <Autocomplete
                    renderInput={(params) => (
                        <TextField
                            inputRef={field.ref}
                            {...params}
                            error={fieldState.invalid}
                            helperText={fieldState.error?.message ?? helperText}
                            label={label}
                            variant={variant}
                        />
                    )}
                    {...autocompleteProps}
                    {...field}
                    onChange={(event, value) => field.onChange(value)}
                />
            )}
        />
    );
};
