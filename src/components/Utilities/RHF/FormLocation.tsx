import { useFormContext } from "react-hook-form";
import { Marker } from "react-leaflet";
import { useRef } from "react";
import L, { LatLng, Marker as LeafletMarker } from "leaflet";
import { Stack, Typography } from "@mui/material";

import { LeafletMap } from "../Leaflet/LeafletMap";

import PinIcon from "~icons/mdi/pin?raw";

export type FormLocationProps = {
    latField: string;
    lonField: string;
};

const formMarker = L.divIcon({
    html: PinIcon as unknown as string,
    iconSize: [42, 42],
    className: "leaflet-marker-location",
});

export const FormLocation = ({
    latField = "coordinate_lat",
    lonField = "coordinate_lon",
}: FormLocationProps) => {
    const { watch, setValue } = useFormContext();

    const markerRef = useRef<LeafletMarker | null>(null);

    const lat: number = watch(latField, 52.006905);
    const lon: number = watch(lonField, 4.360515);

    return (
        <Stack>
            <Typography>
                The coordinates are {lat}, {lon}
            </Typography>
            <LeafletMap sx={{ height: 330 }}>
                <Marker
                    icon={formMarker}
                    ref={markerRef}
                    draggable={true}
                    position={new LatLng(lat, lon)}
                    eventHandlers={{
                        dragend: () => {
                            const marker = markerRef.current;
                            if (marker === null) {
                                return;
                            }
                            setValue(latField, marker.getLatLng().lat);
                            setValue(lonField, marker.getLatLng().lng);
                        },
                    }}
                />
            </LeafletMap>
        </Stack>
    );
};
