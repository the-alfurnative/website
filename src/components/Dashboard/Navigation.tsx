import type { BottomNavigationActionProps } from "@mui/material";
import {
    BottomNavigation,
    BottomNavigationAction,
    Box,
    Collapse,
    Divider,
    Paper,
    styled,
} from "@mui/material";
import { Link, useLocation } from "react-router-dom";
import { useMemo } from "react";
import { useOidc } from "@axa-fr/react-oidc";

import { useHasScrolled } from "../../hooks";
import { useRoles } from "../Authentication/useRoles";

import HomeIcon from "~icons/mdi/paw";
import RAndDIcon from "~icons/material-symbols/science-outline";
import AccountIcon from "~icons/mdi/account";
import AdminIcon from "~icons/mdi/wrench";
import ScheduleIcon from "~icons/mdi/calendar-clock";
const NAV_HEIGHT = 70;

const CustomCollapse = styled(Collapse)({
    position: "fixed",
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 1600,
});

const CustomBottomNavigation = styled(BottomNavigation)({
    height: NAV_HEIGHT,
});

export const BottomNavigationLink = (
    props: Pick<BottomNavigationActionProps, "label" | "icon" | "value">
) => {
    return (
        <BottomNavigationAction {...props} component={Link} to={props.value} />
    );
};

export const Navigation = () => {
    const location = useLocation();
    const hasScrolled = useHasScrolled();
    const { isAuthenticated } = useOidc();
    const roles = useRoles();

    const isStaff = useMemo(() => roles?.includes("Staff") ?? false, [roles]);
    return (
        <CustomCollapse in={hasScrolled}>
            <Divider />
            <Paper elevation={3} role={"navigation"}>
                <CustomBottomNavigation showLabels value={location.pathname}>
                    <BottomNavigationLink
                        label={"Home"}
                        icon={<HomeIcon />}
                        value={"/"}
                    />
                    <BottomNavigationLink
                        value={"/research-and-development"}
                        icon={<RAndDIcon />}
                        label={"R&D"}
                    />
                    <BottomNavigationLink
                        value={"/schedule"}
                        label={"Schedule"}
                        icon={<ScheduleIcon />}
                    />
                    {isAuthenticated && (
                        <BottomNavigationLink
                            icon={<AccountIcon />}
                            label={"Profile"}
                            value={"/account"}
                        />
                    )}
                    {isStaff && (
                        <BottomNavigationLink
                            icon={<AdminIcon />}
                            label={"Admin"}
                            value={"/admin"}
                        />
                    )}
                </CustomBottomNavigation>
            </Paper>
        </CustomCollapse>
    );
};

export const NavigationSpacer = styled(Box)({
    height: NAV_HEIGHT,
    bgcolor: "primary.dark",
});
