import { Box, styled } from "@mui/material";
import { Outlet } from "react-router-dom";

import { useAutoScroll } from "../../hooks";
import { CustomErrorBoundary } from "../ErrorHandling/CustomErrorBoundary";
import { CustomAuthProvider } from "../Authentication/CustomAuthProvider";

import { NavigationSpacer } from "./Navigation";

const DashboardInner = styled(Box)({
    width: "100%",
    display: "flex",
    flexDirection: "column",
});

const MainSection = styled(Box)({
    display: "flex",
    flexDirection: "column",
    flex: 1,
});

export const Dashboard = () => {
    useAutoScroll();
    return (
        <CustomAuthProvider>
            <DashboardInner>
                <MainSection component={"main"} role={"main"}>
                    <CustomErrorBoundary>
                        <Box
                            minHeight={"93vh"}
                            display={"flex"}
                            flexDirection={"column"}
                        >
                            <Outlet />
                        </Box>
                    </CustomErrorBoundary>
                    <NavigationSpacer />
                </MainSection>
            </DashboardInner>
        </CustomAuthProvider>
    );
};
