import { Avatar, Box, Card, CardHeader, Chip, IconButton } from "@mui/material";
import type { OidcUserInfo } from "@axa-fr/react-oidc";

import LogoutIcon from "~icons/mdi/logout";

export const AccountCard = ({
    user,
    logout,
    roles,
}: {
    user: OidcUserInfo;
    logout?: () => void;
    roles?: string[];
}) => {
    return (
        <Card>
            <CardHeader
                avatar={<Avatar src={user?.picture} />}
                title={user?.name}
                subheader={user?.email}
                action={
                    logout && (
                        <IconButton onClick={() => logout()}>
                            <LogoutIcon />
                        </IconButton>
                    )
                }
            />
            {roles && (
                <Box
                    px={2}
                    pb={2}
                    display={"flex"}
                    gap={1}
                    flexDirection={"row"}
                    maxWidth={"100%"}
                >
                    {roles.map((it) => (
                        <Chip
                            key={it}
                            label={it}
                            color={"primary"}
                            variant={"outlined"}
                        />
                    ))}
                </Box>
            )}
        </Card>
    );
};
