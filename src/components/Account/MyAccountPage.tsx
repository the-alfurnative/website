import { Grid } from "@mui/material";
import { useOidc, useOidcUser } from "@axa-fr/react-oidc";
import { useNavigate } from "react-router-dom";

import { useRoles } from "../Authentication/useRoles";

import { AccountCard } from "./AccountCard";

export const MyAccountPage = () => {
    const navigate = useNavigate();
    const { logout } = useOidc();
    const user = useOidcUser();
    const roles = useRoles();

    return (
        <Grid
            container
            spacing={2}
            alignItems={"center"}
            justifyContent={"center"}
            minHeight={"80vh"}
        >
            <Grid item xs={12} md={6} lg={4}>
                <AccountCard
                    user={user.oidcUser}
                    logout={() => {
                        navigate("/");
                        logout("/");
                    }}
                    roles={roles}
                />
            </Grid>
        </Grid>
    );
};
