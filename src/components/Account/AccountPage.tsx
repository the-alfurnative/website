import { Outlet } from "react-router-dom";
import { OidcSecure } from "@axa-fr/react-oidc";

import { Page } from "../Layout/Page";

export const AccountPage = () => {
    return (
        <OidcSecure>
            <Page>
                <Outlet />
            </Page>
        </OidcSecure>
    );
};
