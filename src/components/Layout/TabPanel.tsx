import { PropsWithChildren } from "react";
import { Box } from "@mui/material";

export const TabPanel = <T extends string | number>({
    value,
    index,
    children,
}: PropsWithChildren<TabPanelProps<T>>) =>
    value === index ? (
        <Box role={"tabpanel"} flex={1}>
            {children}
        </Box>
    ) : null;
export type TabPanelProps<T extends string | number> = {
    /**
     * The value associated with this tab
     */
    value: T;
    /**
     * The current value of the tabs component
     */
    index: T;
};
