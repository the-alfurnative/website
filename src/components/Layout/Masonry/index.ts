import { Collapse, styled } from "@mui/material";
import { TransitionGroup } from "react-transition-group";

export const Brick = styled(Collapse)(({ theme }) => ({
    width: "100%",
    display: "inline-block",
    marginBottom: theme.spacing(2),
}));

export const Masonry = styled(TransitionGroup)<{
    columnCount?: number;
    columnWidth?: string | number;
}>(({ theme, columnCount = 3, columnWidth = "450px" }) => ({
    columns: `${columnCount} ${columnWidth}`,
    columnGap: theme.spacing(2),
    maxWidth: "100%",
}));
