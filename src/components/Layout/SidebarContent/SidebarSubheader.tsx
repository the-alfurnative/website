import { Divider, ListSubheader } from "@mui/material";
import type { ListSubheaderProps } from "@mui/material";

export const SidebarSubheader = ({ open, ...props }: SidebarSubheaderProps) => {
    return (
        <>
            <Divider />
            {open && <ListSubheader {...props} />}
        </>
    );
};

export type SidebarSubheaderProps = {
    open: boolean;
} & ListSubheaderProps;
