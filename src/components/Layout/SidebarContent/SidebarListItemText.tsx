import type { ListItemTextProps } from "@mui/material";
import { ListItemText } from "@mui/material";

export const SidebarListItemText = ({
    open,
    ...props
}: SidebarListItemTextProps) => (open ? <ListItemText {...props} /> : null);

export type SidebarListItemTextProps = {
    open: boolean;
} & ListItemTextProps;
