import type { SxProps } from "@mui/material";
import { useTheme, useMediaQuery } from "@mui/material";
import { useCallback, useMemo, useState } from "react";

export type SidebarProps = {
    open: boolean;
    toggle: () => void;
    sidebarSx: SxProps;
    contentSx: SxProps;
    containerSx: SxProps;
};
export const useSidebarProps = () => {
    const theme = useTheme();
    const defaultOpen = useMediaQuery(theme.breakpoints.up("md"));
    const [open, setOpen] = useState(defaultOpen);
    const toggle = useCallback(
        (value?: boolean) =>
            setOpen((draft) => {
                if (value !== undefined) {
                    return value;
                }
                return !draft;
            }),
        [setOpen]
    );

    return useMemo(
        () =>
            ({
                open: open,
                toggle,
                sidebarSx: {
                    width: open ? 300 : 64,
                    flexShrink: 0,
                    flexDirection: "column",
                    borderRightColor: "divider",
                    borderRightWidth: "1px",
                    borderRightStyle: "solid",
                },
                contentSx: {
                    flex: 1,
                    display: "flex",
                    flexDirection: "column",
                    minWidth: 0,
                    minHeight: "100%",
                    maxHeight: "100%",
                },
                containerSx: {
                    display: "flex",
                    flex: 1,
                    flexDirection: "row",
                    bgcolor: "background.paper",
                },
            } as SidebarProps),
        [open, toggle]
    );
};
