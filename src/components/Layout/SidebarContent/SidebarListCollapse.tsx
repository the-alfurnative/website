import { ListItemButton, ListItemIcon } from "@mui/material";

import { SidebarListItemText } from "./SidebarListItemText";

import MenuOpenIcon from "~icons/mdi/menu-open";
import MenuClosedIcon from "~icons/mdi/menu";

export const SidebarListCollapseToggle = ({
    open,
    toggle,
}: SidebarListCollapseToggleProps) => (
    <ListItemButton
        onClick={() => toggle()}
        title={"Open or close the sidebar"}
    >
        <ListItemIcon>
            {open ? <MenuOpenIcon /> : <MenuClosedIcon />}
        </ListItemIcon>
        <SidebarListItemText open={open} primary={"Menu"} />
    </ListItemButton>
);
export type SidebarListCollapseToggleProps = {
    open: boolean;
    toggle: (value?: boolean) => void;
};
