import { Box } from "@mui/material";
import { PropsWithChildren } from "react";

export const Page = ({ children }: PropsWithChildren) => {
    return (
        <Box minHeight={"80vh"} display={"flex"} flexDirection={"column"}>
            {children}
        </Box>
    );
};
