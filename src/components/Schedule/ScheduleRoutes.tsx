import type { RouteObject } from "react-router-dom";

import { createIndexRoute } from "../Utilities/Routing/createIndexRoute";

import { SchedulePage } from "./SchedulePage";

export const ScheduleRoutes = (): RouteObject[] => [
    {
        path: "schedule",
        children: [
            createIndexRoute("list"),
            {
                path: "list",
                element: <SchedulePage />,
            },
        ],
    },
];
