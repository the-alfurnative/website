import dayjs from "dayjs";
import { useMemo } from "react";
import type { DraftFunction } from "use-immer";
import { useImmer } from "use-immer";
import _, { every, some } from "lodash-es";
import { useDebounce } from "ahooks";

import type { PublicEventScheduleItem } from "../../apis";

export type EventFilterState = {
    query: string;
    rooms: string[];
    tracks: string[];
    days: string[];
};

export type UseEventFilterValue = {
    state: EventFilterState;
    setState: (arg: DraftFunction<EventFilterState> | EventFilterState) => void;
    filteredEvents: PublicEventScheduleItem[];

    days: string[];
    tracks: string[];
    rooms: string[];
};

export const useEventFilter = (events: PublicEventScheduleItem[]) => {
    const [state, setState] = useImmer<EventFilterState>({
        query: "",
        rooms: [],
        tracks: [],
        days: [],
    });

    const filtered = useMemo(() => {
        const searchRegex = new RegExp(state.query, "i");

        return events.filter((it) =>
            every([
                state.rooms.length > 0 ? state.rooms.includes(it.room) : true,
                state.days.length > 0
                    ? state.days.includes(dayjs(it.event_start).format("LL"))
                    : true,
                state.tracks.length > 0
                    ? state.tracks.includes(it.track)
                    : true,
                state.query.length > 2
                    ? some([
                          it.description.search(searchRegex) !== -1,
                          it.title.search(searchRegex) !== -1,
                      ])
                    : true,
            ])
        );
    }, [events, state]);

    const debouncedFiltered = useDebounce(filtered, {
        maxWait: 400,
        wait: 200,
    });

    const dayOptions = useMemo(
        () =>
            _.chain(events)
                .map((it: PublicEventScheduleItem) => dayjs(it.event_start))
                .orderBy((it) => it.valueOf(), "asc")
                .map((it) => it.format("LL"))
                .uniq()
                .value(),
        [events]
    );

    const trackOptions = useMemo(() => {
        return _.chain(events)
            .map((it: PublicEventScheduleItem) => it.track)
            .uniq()
            .sort()
            .value();
    }, [events]);
    const roomOptions = useMemo(
        () =>
            _.chain(events)
                .map((it: PublicEventScheduleItem) => it.room)
                .uniq()
                .sort()
                .value(),
        [events]
    );
    return useMemo(
        () =>
            ({
                state,
                filteredEvents: debouncedFiltered,
                setState,
                days: dayOptions,
                tracks: trackOptions,
                rooms: roomOptions,
            } as UseEventFilterValue),
        [state, debouncedFiltered, dayOptions, trackOptions, roomOptions]
    );
};
