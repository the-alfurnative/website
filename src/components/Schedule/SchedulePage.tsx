import {
    Box,
    Checkbox,
    CircularProgress,
    InputAdornment,
    List,
    ListItem,
    ListItemButton,
    ListItemIcon,
    TextField,
    useTheme,
} from "@mui/material";
import { useQuery } from "react-query";
import { useCallback, useState } from "react";
import { remove } from "lodash-es";
import { useDebounce, useUpdateEffect } from "ahooks";

import { backendKeys, getPublicSchedule } from "../../apis";
import {
    SidebarListCollapseToggle,
    SidebarListItemText,
    SidebarSubheader,
    useSidebarProps,
} from "../Layout";

import { ScheduleList } from "./ScheduleList";
import { useEventFilter } from "./useEventFilter";

import SearchIcon from "~icons/mdi/calendar-search";

export const SchedulePage = () => {
    const theme = useTheme();
    const { containerSx, sidebarSx, open, toggle, contentSx } =
        useSidebarProps();

    const { data = [] } = useQuery(backendKeys.publicSchedule(), () =>
        getPublicSchedule().then()
    );

    const { days, state, rooms, tracks, setState, filteredEvents } =
        useEventFilter(data);

    const toggleRoom = useCallback(
        (room: string) => () =>
            setState((draft) => {
                if (draft.rooms.includes(room)) {
                    remove(draft.rooms, (it) => it === room);
                } else {
                    draft.rooms.push(room);
                }
            }),
        [setState]
    );

    const toggleDay = useCallback(
        (day: string) => () =>
            setState((draft) => {
                if (draft.days.includes(day)) {
                    remove(draft.days, (it) => it === day);
                } else {
                    draft.days.push(day);
                }
            }),
        [setState]
    );

    const toggleTrack = useCallback(
        (track: string) => () =>
            setState((draft) => {
                if (draft.tracks.includes(track)) {
                    remove(draft.tracks, (it) => it === track);
                } else {
                    draft.tracks.push(track);
                }
            }),
        [setState]
    );

    const [search, setSearch] = useState(state.query);
    const debouncedSearch = useDebounce(search, { wait: 200 });

    useUpdateEffect(
        () =>
            setState((draft) => {
                draft.query = debouncedSearch;
            }),
        [debouncedSearch]
    );
    return (
        <Box sx={containerSx}>
            <List
                sx={{
                    ...sidebarSx,
                    [theme.breakpoints.down("md")]: {
                        display: "none",
                    },
                }}
            >
                <SidebarListCollapseToggle open={open} toggle={toggle} />
                {open && (
                    <ListItem>
                        <TextField
                            variant={"outlined"}
                            size={"small"}
                            fullWidth
                            placeholder={"Fursuit Parade . . ."}
                            onChange={(e) => setSearch(e.target.value)}
                            value={search}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position={"start"}>
                                        {search === debouncedSearch ? (
                                            <SearchIcon />
                                        ) : (
                                            <CircularProgress
                                                disableShrink={true}
                                                size={20}
                                            />
                                        )}
                                    </InputAdornment>
                                ),
                            }}
                        />
                    </ListItem>
                )}
                <SidebarSubheader open={open}>Days</SidebarSubheader>
                {days.map((it) => (
                    <ListItemButton key={it} onClick={toggleDay(it)}>
                        <ListItemIcon>
                            <Checkbox
                                checked={state.days.includes(it)}
                                edge={"start"}
                                disableRipple
                            />
                        </ListItemIcon>
                        <SidebarListItemText open={open} primary={it} />
                    </ListItemButton>
                ))}
                <SidebarSubheader open={open}>Rooms</SidebarSubheader>
                {rooms.map((it) => {
                    return (
                        <ListItemButton key={it} onClick={toggleRoom(it)}>
                            <ListItemIcon>
                                <Checkbox
                                    edge={"start"}
                                    checked={state.rooms.includes(it)}
                                    disableRipple
                                />
                            </ListItemIcon>
                            <SidebarListItemText open={open} primary={it} />
                        </ListItemButton>
                    );
                })}
                <SidebarSubheader open={open}>Tracks</SidebarSubheader>
                {tracks.map((it) => (
                    <ListItemButton key={it} onClick={toggleTrack(it)}>
                        <ListItemIcon>
                            <Checkbox
                                checked={state.tracks.includes(it)}
                                edge={"start"}
                                disableRipple
                            />
                        </ListItemIcon>
                        <SidebarListItemText open={open} primary={it} />
                    </ListItemButton>
                ))}
            </List>
            <Box
                sx={{
                    ...contentSx,
                    mt: 2,
                    mx: 2,
                }}
            >
                <ScheduleList items={filteredEvents} />
            </Box>
        </Box>
    );
};
