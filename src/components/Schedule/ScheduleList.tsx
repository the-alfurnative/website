import { PublicEventScheduleItem } from "../../apis";
import { Masonry, Brick } from "../Layout";

import { ScheduleItemCard } from "./ScheduleItemCard";
export const ScheduleList = ({ items }: ScheduleListProps) => {
    return (
        <Masonry>
            {items.map((it) => (
                <Brick key={it.id}>
                    <ScheduleItemCard item={it} key={it.id} />
                </Brick>
            ))}
        </Masonry>
    );
};

export type ScheduleListProps = {
    items: PublicEventScheduleItem[];
};
