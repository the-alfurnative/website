import { every, some } from "lodash-es";
import dayjs from "dayjs";

import { PublicEventScheduleItem } from "../../apis";

export type EventFilterState = {
    query: string;
    rooms: string[];
    tracks: string[];
    days: string[];
};

export const filterEvents = (
    events: PublicEventScheduleItem[],
    state: EventFilterState
): PublicEventScheduleItem[] => {
    console.debug("Filtering", events, state);
    const searchRegex = new RegExp(state.query, "i");

    return events.filter((it) =>
        every([
            state.rooms.length > 0 ? state.rooms.includes(it.room) : true,
            state.days.length > 0
                ? state.days.includes(dayjs(it.event_start).format("LL"))
                : true,
            state.tracks.length > 0 ? state.tracks.includes(it.track) : true,
            state.query.length > 2
                ? some([
                      it.description.search(searchRegex) !== -1,
                      it.title.search(searchRegex) !== -1,
                  ])
                : true,
        ])
    );
};
