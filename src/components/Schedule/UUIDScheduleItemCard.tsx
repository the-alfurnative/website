import { useQuery } from "react-query";
import { Alert, CircularProgress, IconButton, Popper } from "@mui/material";
import { useMemo } from "react";
import {
    bindHover,
    bindPopper,
    usePopupState,
} from "material-ui-popup-state/hooks";

import { backendKeys, getPublicSchedule } from "../../apis";

import { ScheduleItemCard } from "./ScheduleItemCard";

import CalendarIcon from "~icons/mdi/calendar";
import BadCalendarIcon from "~icons/mdi/calendar-alert";

export const HoverableUUIDScheduleItemCard = ({
    id = undefined,
}: Partial<UUIDScheduleItemCardProps>) => {
    const popup = usePopupState({ variant: "popper" });

    return (
        <IconButton {...bindHover(popup)}>
            {id ? <CalendarIcon /> : <BadCalendarIcon />}
            <Popper {...bindPopper(popup)} placement={"right-start"}>
                {id !== undefined && <UUIDScheduleItemCard id={id} />}
            </Popper>
        </IconButton>
    );
};

export const UUIDScheduleItemCard = ({ id }: UUIDScheduleItemCardProps) => {
    const { data = [], isFetching } = useQuery(
        backendKeys.publicSchedule(),
        () => getPublicSchedule()
    );

    const item = useMemo(() => {
        return data.find((it) => it.id === id);
    }, [data]);

    if (isFetching) {
        return <CircularProgress />;
    }
    if (item === undefined) {
        return <Alert severity={"error"}>No event was found</Alert>;
    }

    return <ScheduleItemCard item={item} />;
};
export type UUIDScheduleItemCardProps = {
    id: string;
};
