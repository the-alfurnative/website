import {
    Card,
    CardContent,
    CardHeader,
    Chip,
    Collapse,
    Divider,
    IconButton,
    Stack,
    Typography,
} from "@mui/material";
import dayjs from "dayjs";
import { isEmpty } from "lodash-es";
import { useState } from "react";
import { Remark } from "react-remark";

import { PublicEventScheduleItem } from "../../apis";

import RoomIcon from "~icons/mdi/location";
import TrackIcon from "~icons/mdi/account-group";
import DateIcon from "~icons/mdi/calendar";
import StartTimeIcon from "~icons/mdi/timer-sand";
import EndTimeIcon from "~icons/mdi/timer-sand-complete";
import HostIcon from "~icons/mdi/paw";
import CollapseIcon from "~icons/mdi/chevron-up";
import ExpandIcon from "~icons/mdi/chevron-down";
export const ScheduleItemCard = ({ item }: ScheduleItemCardProps) => {
    const [expanded, setExpanded] = useState(true);
    return (
        <Card>
            <CardHeader
                title={item.title}
                subheader={item.subtitle}
                action={
                    <IconButton onClick={() => setExpanded((draft) => !draft)}>
                        {expanded ? <ExpandIcon /> : <CollapseIcon />}
                    </IconButton>
                }
            />
            <CardContent>
                <Typography gutterBottom sx={{ maxLines: 2 }}>
                    {item.short_description ?? item.description}
                </Typography>
            </CardContent>
            <Collapse in={expanded}>
                <CardContent>
                    <Stack spacing={1} pb={1} direction={"row"}>
                        <Chip
                            label={item.room}
                            icon={<RoomIcon />}
                            title={"The location of this event"}
                        />
                        <Chip
                            label={item.track}
                            icon={<TrackIcon />}
                            title={"The associated track for this event"}
                        />
                        <Chip
                            label={dayjs(item.event_start).format("dddd")}
                            title={`This event is on ${dayjs(
                                item.event_start
                            ).format("LL")}`}
                            icon={<DateIcon />}
                        />
                        <Chip
                            label={dayjs(item.event_start).format("LT")}
                            title={`This event starts at  ${dayjs(
                                item.event_start
                            ).format("LLL")}`}
                            icon={<StartTimeIcon />}
                        />
                        <Chip
                            label={dayjs(item.event_end).format("LT")}
                            title={`This event ends at ${dayjs(
                                item.event_end
                            ).format("LLL")}`}
                            icon={<EndTimeIcon />}
                        />
                    </Stack>
                </CardContent>
                <Divider />
                {!isEmpty(item.hosts) && (
                    <>
                        <CardContent>
                            <Typography variant={"h6"} gutterBottom>
                                Hosts
                            </Typography>
                            <Stack spacing={1} direction={"row"}>
                                {item.hosts.map((it) => (
                                    <Chip
                                        label={it}
                                        key={it}
                                        icon={<HostIcon />}
                                    />
                                ))}
                            </Stack>
                        </CardContent>
                        <Divider />
                    </>
                )}

                {!isEmpty(item.flags) && (
                    <>
                        <CardContent>
                            <Typography variant={"h6"} gutterBottom>
                                Special Info
                            </Typography>
                            <Stack spacing={1} direction={"row"}>
                                {item.flags.map((it) => (
                                    <Chip
                                        label={it}
                                        key={it}
                                        color={"warning"}
                                    />
                                ))}
                            </Stack>
                        </CardContent>
                        <Divider />
                    </>
                )}
                <CardContent>
                    <Remark>{item.description}</Remark>
                </CardContent>
            </Collapse>
        </Card>
    );
};

export type ScheduleItemCardProps = {
    item: PublicEventScheduleItem;
};
