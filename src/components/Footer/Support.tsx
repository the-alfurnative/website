import {
    Link as MuiLink,
    List,
    ListItem,
    ListItemButton,
    ListItemIcon,
    ListItemText,
} from "@mui/material";
import { Link } from "react-router-dom";

import AdminIcon from "~icons/mdi/security";
import PawIcon from "~icons/mdi/paw";

export const Support = () => (
    <List dense>
        <ListItem>
            <ListItemText
                primary={"Support"}
                primaryTypographyProps={{ variant: "h4" }}
            />
        </ListItem>
        <ListItemButton
            component={MuiLink}
            href={"https://universe.com/"}
            rel={"noopener"}
            target={"_blank"}
        >
            <ListItemIcon>
                <PawIcon />
            </ListItemIcon>
            <ListItemText
                primary={"Universe.com"}
                secondary={"Our ticket provider."}
            />
        </ListItemButton>
        <ListItemButton component={Link} to={"/legal"}>
            <ListItemIcon>
                <PawIcon />
            </ListItemIcon>
            <ListItemText
                primary={"Rules"}
                secondary={"Code of Conduct, Privacy Policy and more"}
            />
        </ListItemButton>
        <ListItemButton component={Link} to={"/admin"}>
            <ListItemIcon>
                <AdminIcon />
            </ListItemIcon>
            <ListItemText
                primary={"Staff Portal"}
                secondary={"Access the staff-only area of the Alfurnative."}
            />
        </ListItemButton>
    </List>
);
