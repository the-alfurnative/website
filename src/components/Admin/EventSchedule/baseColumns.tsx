import type { GridColDef } from "@mui/x-data-grid";

import type {
    BackendAdminReadable,
    BackendAdminWritable,
    BackendBaseReadable,
} from "../../../apis";

export const BaseColumns = <
    T extends BackendBaseReadable & BackendAdminReadable & BackendAdminWritable
>(): GridColDef<T>[] => [
    {
        field: "id",
        width: 300,
        headerName: "ID",
    },
    {
        field: "order",
        headerName: "Order",
        type: "number",
        width: 120,
        editable: true,
    },
    {
        field: "notes",
        editable: true,
        headerName: "Orga Notes",
        flex: 1,
        minWidth: 400,
    },
];
