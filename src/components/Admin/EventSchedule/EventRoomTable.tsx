import type { GridColDef } from "@mui/x-data-grid";
import { DataGrid } from "@mui/x-data-grid";
import { useMutation, useQuery } from "react-query";

import {
    backendKeys,
    createScheduleRoom,
    getScheduleRooms,
    ScheduleRoom,
    updateScheduleRoom,
} from "../../../apis";
import { unwrapAxios } from "../../../utilities/axios";
import { useMutationResponse } from "../../../hooks/useMutationResponse";

import { ScheduleToolbar } from "./ScheduleToolbar";
import { BaseColumns } from "./baseColumns";

const eventRoomColumns: GridColDef<ScheduleRoom>[] = [
    {
        field: "name",
        width: 350,
        editable: true,
        headerName: "Name",
    },
    ...BaseColumns<ScheduleRoom>(),
];

export const EventRoomsTable = () => {
    const { data = [] } = useQuery(backendKeys.scheduleRooms(), () =>
        getScheduleRooms().then(unwrapAxios)
    );
    const createMutation = useMutation(
        () => createScheduleRoom(),
        useMutationResponse(
            "Created a new room",
            "Failed to create a new room",
            [backendKeys.scheduleRooms()]
        )
    );
    const updateMutation = useMutation(
        (row: ScheduleRoom) => updateScheduleRoom(row).then(unwrapAxios),
        useMutationResponse(
            "Updated the room entry",
            "Failed to update the room entry",
            [backendKeys.scheduleRooms()]
        )
    );
    return (
        <DataGrid<ScheduleRoom>
            columns={eventRoomColumns}
            rows={data}
            processRowUpdate={(newRow) => updateMutation.mutateAsync(newRow)}
            slots={{ toolbar: ScheduleToolbar }}
            slotProps={{
                toolbar: {
                    mutation: createMutation,
                },
            }}
        />
    );
};
