import {
    GridToolbarColumnsButton,
    GridToolbarContainer,
    GridToolbarFilterButton,
} from "@mui/x-data-grid-pro";
import { Button } from "@mui/material";
import { UseMutationResult } from "react-query";

import PlusIcon from "~icons/mdi/plus";

export const ScheduleToolbar = ({ mutation }: ScheduleToolbarProps) => {
    return (
        <GridToolbarContainer>
            <GridToolbarColumnsButton />
            <GridToolbarFilterButton />
            <Button
                startIcon={<PlusIcon />}
                onClick={mutation.mutate}
                disabled={mutation.status === "loading"}
                size={"small"}
                color={"primary"}
            >
                Add new row
            </Button>
        </GridToolbarContainer>
    );
};
export type ScheduleToolbarProps = {
    mutation: UseMutationResult;
};
