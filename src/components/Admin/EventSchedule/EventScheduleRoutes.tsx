import type { RouteObject } from "react-router-dom";

import { EventSchedulerPage } from "./EventSchedulerPage";
import { EventScheduleTable } from "./EventScheduleTable";
import { EventRoomsTable } from "./EventRoomTable";
import { EventTracksTable } from "./EventTracksTable";

export const EventScheduleRoutes: RouteObject = {
    path: "schedule",
    element: <EventSchedulerPage />,
    children: [
        { index: true, element: <EventScheduleTable /> },
        { path: "events", element: <EventScheduleTable /> },
        { path: "rooms", element: <EventRoomsTable /> },
        { path: "tracks", element: <EventTracksTable /> },
    ],
};
