import type { GridColDef } from "@mui/x-data-grid";
import { DataGrid } from "@mui/x-data-grid";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useSnackbar } from "notistack";

import {
    backendKeys,
    createScheduleTrack,
    getScheduleTracks,
    ScheduleRoom,
    ScheduleTrack,
    updateScheduleTrack,
} from "../../../apis";
import { unwrapAxios } from "../../../utilities/axios";

import { ScheduleToolbar } from "./ScheduleToolbar";
import { BaseColumns } from "./baseColumns";

const eventTracksColumns: GridColDef<ScheduleRoom>[] = [
    {
        field: "name",
        width: 350,
        editable: true,
        headerName: "Name",
    },
    {
        field: "description",
        editable: true,
        headerName: "Description",
        minWidth: 400,
        flex: 1,
    },
    ...BaseColumns<ScheduleTrack>(),
];

export const EventTracksTable = () => {
    const { enqueueSnackbar } = useSnackbar();
    const queryClient = useQueryClient();
    const { data = [] } = useQuery(backendKeys.scheduleTracks(), () =>
        getScheduleTracks().then(unwrapAxios)
    );
    const createMutation = useMutation(() => createScheduleTrack(), {
        onSuccess: () => {
            enqueueSnackbar("Successfully created a new track", {
                variant: "success",
            });
            return queryClient.invalidateQueries(backendKeys.scheduleTracks());
        },
        onError: async () => {
            enqueueSnackbar("Failed to create a new track", {
                variant: "error",
            });
        },
    });

    const updateMutation = useMutation(updateScheduleTrack, {
        onSuccess: async () => {
            enqueueSnackbar("Successfully updated track", {
                variant: "success",
            });
            return queryClient.invalidateQueries(backendKeys.scheduleTracks());
        },
        onError: async () => {
            enqueueSnackbar("Failed to update track", { variant: "error" });
        },
    });
    return (
        <DataGrid<ScheduleRoom>
            columns={eventTracksColumns}
            rows={data}
            processRowUpdate={(newRow) =>
                updateMutation.mutateAsync(newRow).then(unwrapAxios)
            }
            slots={{ toolbar: ScheduleToolbar }}
            slotProps={{
                toolbar: {
                    mutation: createMutation,
                },
            }}
        />
    );
};
