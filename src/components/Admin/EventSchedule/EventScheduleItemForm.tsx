import { FormProvider, useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import {
    Box,
    Button,
    ButtonGroup,
    CardActions,
    CardContent,
    Stack,
    Typography,
} from "@mui/material";
import { useQuery } from "react-query";

import {
    backendKeys,
    getScheduleRooms,
    getScheduleTracks,
    ScheduleEvent,
    ScheduleEventWrite,
} from "../../../apis";
import {
    FormAutocomplete,
    FormDateTimePicker,
    FormMarkdownField,
    FormTextField,
} from "../../Utilities";
import { unwrapAxios } from "../../../utilities";
import { FormFlag } from "../../Utilities/RHF/FormFlag";

export const EventScheduleItemForm = ({
    item,
    onSubmit,
}: EventScheduleItemFormProps) => {
    const form = useForm<ScheduleEventWrite>({
        defaultValues: item ?? {},
        resolver: zodResolver(ScheduleEventWrite),
    });

    const { data: rooms = [], isFetching: roomsLoading } = useQuery(
        backendKeys.scheduleRooms(),
        () => getScheduleRooms().then(unwrapAxios)
    );

    const { data: tracks = [], isFetching: tracksFetching } = useQuery(
        backendKeys.scheduleTracks(),
        () => getScheduleTracks().then(unwrapAxios)
    );

    return (
        <>
            <CardContent sx={{ overflowY: "auto", maxHeight: "70vh" }}>
                <FormProvider {...form}>
                    <Stack spacing={2}>
                        <Typography variant={"h5"}>Text Fields</Typography>
                        <FormTextField<ScheduleEventWrite>
                            name={"title"}
                            variant={"filled"}
                            label={"Title"}
                            helperText={
                                "A clear an intuit-able name for this event"
                            }
                            required
                        />
                        <FormTextField<ScheduleEventWrite>
                            name={"subtitle"}
                            variant={"filled"}
                            label={"Subtitle"}
                            helperText={"A catchy tagline for the event!"}
                        />
                        <FormTextField<ScheduleEventWrite>
                            name={"short_description"}
                            variant={"filled"}
                            label={"Short Description"}
                            helperText={
                                "A shorter version of the description field. Can be used when data is being truncated"
                            }
                        />
                        <FormMarkdownField<ScheduleEventWrite>
                            required
                            name={"description"}
                            variant={"filled"}
                            label={"Description"}
                            helperText={
                                "Give an in-depth description. What can attendees expect? You can write markdown!"
                            }
                        />

                        <Typography variant={"h5"}>Metadata</Typography>
                        <FormAutocomplete<ScheduleEventWrite>
                            name={"room_uuid"}
                            label={"Room"}
                            helperText={"Where will this event take place?"}
                            options={rooms.map((it) => it.id)}
                            getOptionLabel={(option) =>
                                rooms.find((it) => it.id === option)?.name ??
                                option
                            }
                            loading={roomsLoading}
                        />
                        <FormAutocomplete<ScheduleEventWrite>
                            name={"track_uuid"}
                            options={tracks.map((it) => it.id)}
                            label={"Track"}
                            helperText={
                                "What category does this event belong to?"
                            }
                            getOptionLabel={(option) =>
                                tracks.find((it) => it.id === option)?.name ??
                                option
                            }
                            loading={tracksFetching}
                        />
                        <FormAutocomplete<ScheduleEventWrite>
                            name={"hosts"}
                            freeSolo
                            options={[]}
                            multiple
                            label={"Hosts"}
                            helperText={"People who are organizing this event?"}
                        />
                        <Box
                            display={"flex"}
                            flexDirection={"row"}
                            gap={2}
                            justifyContent={"stretch"}
                            alignItems={"stretch"}
                        >
                            <FormDateTimePicker<ScheduleEventWrite>
                                name={"event_start"}
                                label={"Event Start"}
                                slotProps={{
                                    textField: {
                                        fullWidth: true,
                                    },
                                }}
                                sx={{ flex: 1 }}
                            />
                            <FormDateTimePicker<ScheduleEventWrite>
                                name={"event_end"}
                                label={"Event End"}
                                sx={{ flex: 1 }}
                            />
                        </Box>
                        <FormFlag
                            name={"flags"}
                            flag={"After Dark"}
                            label={"This event is After Dark"}
                        />
                        <Typography variant={"h5"}>Internal stuff</Typography>
                        <FormTextField
                            name={"notes"}
                            label={"Orga notes"}
                            multiline
                            rows={8}
                        />
                    </Stack>
                </FormProvider>
            </CardContent>
            <CardActions>
                <ButtonGroup variant={"outlined"} fullWidth>
                    <Button color={"error"} disabled>
                        Delete
                    </Button>
                    <Button disabled>Publish</Button>
                    <Button disabled>Unpublish</Button>

                    <Button
                        color={"success"}
                        onClick={form.handleSubmit(onSubmit, console.error)}
                    >
                        Save Event
                    </Button>
                </ButtonGroup>
            </CardActions>
        </>
    );
};

export type EventScheduleItemFormProps = {
    item?: ScheduleEvent | ScheduleEventWrite;
    onSubmit: (item: ScheduleEventWrite) => Promise<void>;
};
