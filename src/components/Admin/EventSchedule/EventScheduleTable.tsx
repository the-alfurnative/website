import { useMutation, useQuery } from "react-query";
import type { GridColDef } from "@mui/x-data-grid";
import { DataGrid } from "@mui/x-data-grid";
import { useMemo } from "react";
import { GridActionsCellItem, GridEditInputCell } from "@mui/x-data-grid-pro";
import dayjs from "dayjs";
import { Chip } from "@mui/material";
import { every, isEmpty, isNil, uniq } from "lodash-es";
import { match, P } from "ts-pattern";

import {
    backendKeys,
    createNewEvent,
    deleteEvent,
    getScheduleEvents,
    getScheduleRooms,
    getScheduleTracks,
    publishEvent,
    ScheduleEvent,
    unpublishEvent,
    updateEvent,
} from "../../../apis";
import { unwrapAxios } from "../../../utilities/axios";
import { DateTimeColumn } from "../../Utilities";
import { HoverableUUIDScheduleItemCard } from "../../Schedule";
import { useMutationResponse } from "../../../hooks/useMutationResponse";

import { ScheduleToolbar } from "./ScheduleToolbar";
import { BaseColumns } from "./baseColumns";
import { EventScheduleItemEditor } from "./EventScheduleItemEditor";

import DeleteIcon from "~icons/mdi/delete";
import PublishIcon from "~icons/mdi/calendar-plus";

export const EventScheduleTable = () => {
    const { data = [], isFetching } = useQuery(
        backendKeys.scheduleEvents(),
        () => getScheduleEvents().then((data) => data.data)
    );
    const columns = useScheduleColumns();
    const createNewRow = useMutation(
        () => createNewEvent(),
        useMutationResponse(
            "Created a new event",
            "Failed to create a new event",
            [backendKeys.scheduleEvents()]
        )
    );
    const updateRow = useMutation(
        (newData: ScheduleEvent) => updateEvent(newData).then(unwrapAxios),
        useMutationResponse(
            "Updated the event row",
            "Failed to update the event",
            [backendKeys.scheduleEvents()]
        )
    );
    return (
        <DataGrid<ScheduleEvent>
            initialState={{
                columns: {
                    columnVisibilityModel: {
                        id: false,
                        internal_name: false,
                    },
                },
            }}
            editMode={"row"}
            loading={isFetching || updateRow.status === "loading"}
            rows={data}
            columns={columns}
            processRowUpdate={(newRow) => updateRow.mutateAsync(newRow)}
            slots={{ toolbar: ScheduleToolbar }}
            slotProps={{
                toolbar: {
                    mutation: createNewRow,
                },
            }}
        />
    );
};

export const useScheduleColumns = () => {
    const { data: rooms = [] } = useQuery(backendKeys.scheduleRooms(), () =>
        getScheduleRooms().then((data) => data.data)
    );
    const { data: tracks = [] } = useQuery(backendKeys.scheduleTracks(), () =>
        getScheduleTracks().then((data) => data.data)
    );
    const deleteMutation = useMutation(
        deleteEvent,
        useMutationResponse(
            "Deleted this event completely",
            "Failed to delete this event",
            [backendKeys.scheduleEvents(), backendKeys.publicSchedule()]
        )
    );

    const publishMutation = useMutation(
        publishEvent,
        useMutationResponse(
            "Successfully published/updated the public event!",
            "Failed to update the public event!",
            [backendKeys.publicSchedule(), backendKeys.scheduleEvents()]
        )
    );

    const unpublishMutation = useMutation(
        unpublishEvent,
        useMutationResponse(
            "This event has been removed from the schedule",
            "Failed to remove this event from the schedule!",
            [backendKeys.publicSchedule(), backendKeys.scheduleEvents()]
        )
    );
    return useMemo(
        (): GridColDef<ScheduleEvent>[] => [
            {
                field: "edit",
                type: "string",
                valueGetter: () => undefined,
                renderCell: ({ row }) => <EventScheduleItemEditor item={row} />,
            },
            {
                field: "actions",
                type: "actions",
                width: 75,
                getActions: (params) => [
                    <GridActionsCellItem
                        label={
                            params.row.published_event_id === undefined
                                ? "Publish this event"
                                : "Update the public schedule"
                        }
                        onClick={() => publishMutation.mutate(params.row.id)}
                        icon={<PublishIcon />}
                        showInMenu
                    />,
                    <GridActionsCellItem
                        label={"Remove this from the public schedule"}
                        disabled={params.row.published_event_id === undefined}
                        onClick={() => unpublishMutation.mutate(params.row.id)}
                        icon={<PublishIcon />}
                        showInMenu
                    />,
                    <GridActionsCellItem
                        label={"Delete this event completely"}
                        onClick={() => deleteMutation.mutate(params.row.id)}
                        icon={<DeleteIcon />}
                        showInMenu={true}
                    />,
                ],
                headerName: "Actions",
            },
            {
                field: "published_event_id",
                type: "boolean",
                width: 120,
                valueGetter: ({ row }) => row.published_event_id !== null,
                headerName: "Published",
                disableColumnMenu: true,
                renderCell: (params) =>
                    params.row.published_event_id && (
                        <HoverableUUIDScheduleItemCard
                            id={params.row.published_event_id}
                        />
                    ),
            },
            {
                field: "ready_to_publish",
                headerName: "Valid",
                description: "This event is valid and can be published",
                type: "boolean",
                width: 120,
                valueGetter: ({ row }) =>
                    every(
                        [
                            row.title,
                            row.description,
                            row.room_uuid,
                            row.track_uuid,
                            row.event_end,
                            row.event_start,
                        ],
                        (it) => !isNil(it)
                    ),
            },
            {
                field: "title",
                type: "string",
                width: 250,
                headerName: "Title*",
                editable: true,
            },
            {
                field: "subtitle",
                type: "string",
                width: 250,
                headerName: "Subtitle",
                editable: true,
            },
            {
                field: "description",
                type: "string",
                width: 300,
                headerName: "Description*",
                editable: true,
            },
            {
                field: "short_description",
                type: "string",
                width: 250,
                headerName: "Short Description",
                editable: true,
            },
            {
                field: "room_uuid",
                type: "singleSelect",
                width: 175,
                headerName: "Room*",
                editable: true,
                valueOptions: rooms.map((it) => ({
                    value: it.id,
                    label: it.name,
                })),
            },
            {
                field: "track_uuid",
                type: "singleSelect",
                width: 175,
                headerName: "Track*",
                editable: true,
                valueOptions: tracks.map((it) => ({
                    value: it.id,
                    label: it.name,
                })),
            },
            {
                field: "hosts",
                headerName: "Hosts",
                editable: true,
                width: 300,
                valueGetter: ({ row }) => row.hosts ?? [],
                valueFormatter: ({ value }) => value?.join(" "),
                renderCell: ({ value }) => (
                    <>
                        {value?.map((it) => (
                            <Chip label={it} key={it} />
                        ))}
                    </>
                ),
                renderEditCell: (params) => <GridEditInputCell {...params} />,
                valueSetter: (params) => {
                    return {
                        ...params.row,
                        hosts: match(params.value as unknown)
                            .with(P.string, (res) =>
                                res
                                    .split(",")
                                    .map((it) => it.trim())
                                    .filter((it) => !isEmpty(it))
                            )
                            .with(undefined, () => [])
                            .otherwise((r) => r),
                    };
                },
            } as GridColDef<ScheduleEvent, string[], string>,
            {
                field: "event_start",
                type: "dateTime",
                width: 200,
                headerName: "Event Start*",
                editable: true,
                valueGetter: ({ row }) =>
                    row.event_start
                        ? dayjs(row.event_start).toDate()
                        : undefined,
                ...DateTimeColumn,
            } as GridColDef<ScheduleEvent, Date | undefined, string>,
            {
                field: "event_end",
                type: "dateTime",
                width: 200,
                headerName: "Event End*",
                editable: true,
                valueGetter: ({ row }) =>
                    row.event_end ? dayjs(row.event_end).toDate() : undefined,

                ...DateTimeColumn,
            } as GridColDef<ScheduleEvent, Date | undefined, string>,
            {
                field: "after_dark",
                headerName: "After Dark",
                type: "boolean",
                editable: true,
                width: 150,
                valueGetter: ({ row }) =>
                    row.flags.find((it) => it === "After Dark") !== undefined,
                valueSetter: (params) => ({
                    ...params.row,
                    flags: params.value
                        ? uniq([...params.row.flags, "After Dark"])
                        : uniq(
                              params.row.flags.filter(
                                  (it) => it !== "After Dark"
                              )
                          ),
                }),
            } as GridColDef<ScheduleEvent, boolean, boolean>,
            ...BaseColumns<ScheduleEvent>(),
        ],
        [rooms, tracks, deleteMutation]
    );
};
