import { Box, Divider, Tab, Tabs } from "@mui/material";
import { Outlet, useMatch, useNavigate } from "react-router-dom";
import { useEffect } from "react";

import { EventScheduleEditorRoutes } from "./routes";

export const EventSchedulerPage = () => {
    const navigate = useNavigate();
    const match = useMatch("/admin/schedule/:part");

    useEffect(() => console.debug(match), [match]);
    return (
        <>
            <Tabs
                variant={"fullWidth"}
                value={match?.params?.part ?? "events"}
                color={"primary"}
            >
                {(EventScheduleEditorRoutes()[0]?.children ?? [])
                    .filter((it) => it.name !== undefined)
                    .map((it) => (
                        <Tab
                            label={it.name}
                            key={it.path}
                            value={it.path}
                            component="a"
                            onClick={(e: any) => {
                                e.preventDefault();
                                it.path && navigate(it.path);
                            }}
                        />
                    ))}
            </Tabs>
            <Divider />
            <Box p={3} flex={1} display={"flex"} flexDirection={"row"}>
                <Outlet />
            </Box>
        </>
    );
};
