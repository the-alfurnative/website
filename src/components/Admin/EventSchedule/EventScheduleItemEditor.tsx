import { Card, CardHeader, Dialog, IconButton } from "@mui/material";
import {
    bindDialog,
    bindTrigger,
    usePopupState,
} from "material-ui-popup-state/hooks";
import { useMutation } from "react-query";

import {
    backendKeys,
    ScheduleEvent,
    ScheduleEventWrite,
    updateEvent,
} from "../../../apis";
import { useMutationResponse } from "../../../hooks/useMutationResponse";

import { EventScheduleItemForm } from "./EventScheduleItemForm";

import EditIcon from "~icons/mdi/edit";
import CloseIcon from "~icons/mdi/close";

export type EventScheduleItemDialogButton = {
    item: ScheduleEvent | ScheduleEventWrite;
};
export const EventScheduleItemEditor = ({
    item,
}: EventScheduleItemDialogButton) => {
    const dialog = usePopupState({ variant: "dialog" });
    const updateMutation = useMutation(
        (data: ScheduleEventWrite) => updateEvent(data),
        useMutationResponse(
            "Succesfully updated schedule item",
            "Failed to update schedule item",
            [backendKeys.scheduleEvents()]
        )
    );
    return (
        <>
            <IconButton {...bindTrigger(dialog)}>
                <EditIcon />
            </IconButton>
            <Dialog {...bindDialog(dialog)} maxWidth={"lg"}>
                <Card>
                    <CardHeader
                        title={"Edit Schedule Item"}
                        action={
                            <IconButton onClick={dialog.close}>
                                <CloseIcon />
                            </IconButton>
                        }
                    />
                    <EventScheduleItemForm
                        item={item}
                        onSubmit={(data) =>
                            updateMutation
                                .mutateAsync(data)
                                .then(() => dialog.close())
                        }
                    />
                </Card>
            </Dialog>
        </>
    );
};
