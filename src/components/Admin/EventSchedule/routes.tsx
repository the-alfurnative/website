import { CustomRouteObject } from "../../Utilities";
import { createIndexRoute } from "../../Utilities/Routing/createIndexRoute";

import { EventRoomsTable } from "./EventRoomTable";
import { EventScheduleTable } from "./EventScheduleTable";
import { EventTracksTable } from "./EventTracksTable";
import { EventSchedulerPage } from "./EventSchedulerPage";

import ScheduleEditIcon from "~icons/mdi/calendar-edit";

export const EventScheduleEditorRoutes = (): CustomRouteObject[] => [
    {
        path: "schedule",
        name: "Schedule Editor",
        stopIndex: true,
        icon: <ScheduleEditIcon />,
        element: <EventSchedulerPage />,
        children: [
            createIndexRoute("events"),
            {
                path: "events",
                name: "Events",
                element: <EventScheduleTable />,
            },
            {
                path: "rooms",
                name: "Rooms",
                element: <EventRoomsTable />,
            },
            {
                path: "tracks",
                name: "Tracks",
                element: <EventTracksTable />,
            },
        ],
    },
];
