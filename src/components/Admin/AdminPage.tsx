import { Box, Divider, List } from "@mui/material";
import { Outlet } from "react-router-dom";

import { RolesRequired } from "../Authentication/RolesRequired";
import {
    SidebarListCollapseToggle,
    useSidebarProps,
} from "../Layout/SidebarContent";
import { ListItemRoutes } from "../Utilities";

import { AdminRoutes } from "./routes";

export const AdminPage = () => {
    const { containerSx, contentSx, sidebarSx, open, toggle } =
        useSidebarProps();
    return (
        <RolesRequired roles={["Staff", "Developer", "Orga"]}>
            <Box sx={containerSx}>
                <List sx={sidebarSx}>
                    <SidebarListCollapseToggle open={open} toggle={toggle} />

                    <ListItemRoutes routes={AdminRoutes()} open={open} />
                </List>
                <Divider orientation={"vertical"} />
                <Box sx={contentSx}>
                    <Outlet />
                </Box>
            </Box>
        </RolesRequired>
    );
};
