import {
    Card,
    CardContent,
    CardHeader,
    Checkbox,
    IconButton,
} from "@mui/material";
import { useMutation } from "react-query";
import { Remark } from "react-remark";
import { useBoolean } from "ahooks";

import { deleteFAQItem, updateFAQItem } from "../../../apis/backend/faq";
import { useMutationResponse } from "../../../hooks/useMutationResponse";
import { backendKeys } from "../../../apis";
import { FAQQuestion } from "../../../apis/backend/models/FAQQuestion";

import { FAQQuestionForm } from "./FAQQuestionForm";

import EditIcon from "~icons/mdi/edit";
import CloseIcon from "~icons/mdi/close";

export const FAQItemCard = ({ question }: FAQItemCardProps) => {
    const [editing, { toggle, set }] = useBoolean(false);

    const updateMutation = useMutation(
        updateFAQItem,
        useMutationResponse(
            "Successfully updated FAQ item",
            "Failed to update FAQ item",
            [backendKeys.questionsFAQ()]
        )
    );

    const deleteMutation = useMutation(
        deleteFAQItem,
        useMutationResponse(
            "Removed FAQ item from database",
            "Failed to remove FAQ item from database",
            [backendKeys.questionsFAQ()]
        )
    );
    return (
        <Card>
            <CardHeader
                title={question.question}
                subheader={question.category}
                avatar={<Checkbox disabled checked={question.public} />}
                action={
                    <IconButton
                        onClick={toggle}
                        color={question.public ? "success" : "warning"}
                        title={
                            question.public
                                ? "Item is published"
                                : "Item is not published"
                        }
                    >
                        {editing ? <CloseIcon /> : <EditIcon />}
                    </IconButton>
                }
            />
            <CardContent>
                {editing ? (
                    <FAQQuestionForm
                        onSubmit={(data) =>
                            updateMutation
                                .mutateAsync(data)
                                .then(() => set(false))
                        }
                        onDelete={deleteMutation.mutateAsync}
                        question={question}
                    />
                ) : (
                    <Remark>{question.answer}</Remark>
                )}
            </CardContent>
        </Card>
    );
};

export type FAQItemCardProps = {
    question: FAQQuestion;
};
