import { CustomRouteObject } from "../../Utilities";
import { RolesRequired } from "../../Authentication/RolesRequired";

import { FAQEditorPage } from "./FAQEditorPage";

import FAQIcon from "~icons/mdi/faq";
export const FAQEditorRoutes = (): CustomRouteObject[] => [
    {
        name: "FAQ Editor",
        path: "faq",
        icon: <FAQIcon />,
        element: (
            <RolesRequired roles={["Staff"]}>
                <FAQEditorPage />
            </RolesRequired>
        ),
    },
];
