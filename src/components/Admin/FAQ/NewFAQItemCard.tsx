import { Card, CardContent, CardHeader, Divider } from "@mui/material";
import { useMutation } from "react-query";

import { createNewFAQItem } from "../../../apis/backend/faq";
import { useMutationResponse } from "../../../hooks/useMutationResponse";
import { backendKeys, FAQQuestionWrite } from "../../../apis";

import { FAQQuestionForm } from "./FAQQuestionForm";

export const NewFAQItemCard = () => {
    const createMutation = useMutation(
        (item: FAQQuestionWrite) => createNewFAQItem(item),
        useMutationResponse(
            "Created a new FAQ item!",
            "Failed to create new FAQ item",
            [backendKeys.questionsFAQ()]
        )
    );
    return (
        <Card>
            <CardHeader title={"New Question"} />
            <Divider />
            <CardContent>
                <FAQQuestionForm
                    onSubmit={(data) => createMutation.mutateAsync(data)}
                />
            </CardContent>
        </Card>
    );
};
