import { useQuery } from "react-query";

import { Brick, Masonry } from "../../Layout/Masonry";
import { backendKeys } from "../../../apis";
import { getFAQItems } from "../../../apis/backend/faq";
import { unwrapAxios } from "../../../utilities/axios";

import { NewFAQItemCard } from "./NewFAQItemCard";
import { FAQItemCard } from "./FAQItemCard";

export const FAQEditorPage = () => {
    const { data = [] } = useQuery(backendKeys.questionsFAQ(), () =>
        getFAQItems().then(unwrapAxios)
    );
    return (
        <Masonry sx={{ p: 4 }}>
            <Brick>
                <NewFAQItemCard />
            </Brick>
            {data.map((it) => (
                <Brick key={it.id}>
                    <FAQItemCard question={it} />
                </Brick>
            ))}
        </Masonry>
    );
};
