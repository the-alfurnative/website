import { FormProvider, useForm } from "react-hook-form";
import { Box, Button, ButtonGroup, Stack } from "@mui/material";
import { zodResolver } from "@hookform/resolvers/zod";
import { useEffect } from "react";
import { useSnackbar } from "notistack";

import {
    FormTextField,
    FormMarkdownField,
    FormDateTimePicker,
    FormCheckbox,
} from "../../Utilities";
import { FAQQuestionWrite } from "../../../apis/backend/models/FAQQuestionWrite";
import { FAQQuestion } from "../../../apis/backend/models/FAQQuestion";

export const FAQQuestionForm = ({
    question,
    onSubmit,
    onDelete,
}: FAQQuestionFormProps) => {
    const { enqueueSnackbar } = useSnackbar();
    const form = useForm<FAQQuestionWrite>({
        defaultValues: question ?? {
            id: crypto.randomUUID(),
            public: false,
            question: "",
            answer: "",
            order: 100,
            category: "",
        },
        resolver: zodResolver(FAQQuestionWrite),
    });

    useEffect(() => {
        console.debug(form.formState.errors);
    }, [form]);

    return (
        <FormProvider {...form}>
            <Stack spacing={2}>
                <Box
                    display={"flex"}
                    flexDirection={"row"}
                    gap={2}
                    maxWidth={"100%"}
                >
                    <FormTextField
                        name={"id"}
                        label={"ID"}
                        disabled={true}
                        fullWidth={true}
                        variant={"standard"}
                    />

                    <FormTextField
                        name={"internal_name"}
                        label={"ID"}
                        disabled={true}
                        fullWidth={true}
                        variant={"standard"}
                    />
                </Box>
                <Box
                    display={"flex"}
                    flexDirection={"row"}
                    gap={2}
                    maxWidth={"100%"}
                >
                    <FormDateTimePicker
                        name={"date_created"}
                        format={"LLLL"}
                        label={"Edited"}
                        disabled={true}
                        slotProps={{
                            textField: {
                                variant: "standard",
                                fullWidth: true,
                            },
                        }}
                    />
                    <FormDateTimePicker
                        name={"date_edited"}
                        format={"LLLL"}
                        label={"Edited"}
                        disabled={true}
                        slotProps={{
                            textField: {
                                variant: "standard",
                                fullWidth: true,
                            },
                        }}
                    />
                </Box>

                <FormTextField
                    name={"category"}
                    label={"Category"}
                    variant={"filled"}
                />

                <FormTextField
                    name={"question"}
                    label={"Question"}
                    variant={"filled"}
                />

                <FormMarkdownField
                    name={"answer"}
                    variant={"filled"}
                    label={"Answer"}
                    rows={8}
                    helperText={
                        "Give an answer to the question. Markdown is supported"
                    }
                />

                <FormCheckbox
                    name={"public"}
                    label={"Show this item on the public page"}
                />

                <ButtonGroup fullWidth>
                    {onDelete && question && (
                        <Button
                            onClick={() => onDelete(question)}
                            color={"warning"}
                        >
                            Delete this Question
                        </Button>
                    )}
                    <Button
                        onClick={form.handleSubmit(
                            (data) =>
                                onSubmit(data).then(() =>
                                    form.reset(
                                        {
                                            id: crypto.randomUUID(),
                                            public: false,
                                            question: "",
                                            answer: "",
                                            order: 100,
                                            category: data.category,
                                        },
                                        {}
                                    )
                                ),
                            (e) => {
                                console.debug(e);
                                enqueueSnackbar(
                                    `There are still some errors in this form. ${e.root?.message}`
                                );
                            }
                        )}
                        color={"success"}
                    >
                        Save this item
                    </Button>
                </ButtonGroup>
            </Stack>
        </FormProvider>
    );
};
export type FAQQuestionFormProps = {
    question?: FAQQuestionWrite | FAQQuestion;
    onSubmit: (question: FAQQuestionWrite) => Promise<void>;
    onDelete?: (question: FAQQuestionWrite) => Promise<void>;
};
