import { CustomRouteObject } from "../Utilities";
import { createIndexRoute } from "../Utilities/Routing/createIndexRoute";

import { AdminPage } from "./AdminPage";
import { EventScheduleEditorRoutes } from "./EventSchedule/routes";
import { AdminIndexPage } from "./AdminIndexPage";
import { FAQEditorRoutes } from "./FAQ";
import { LocationForm } from "./Location/Location";

import InfoIcon from "~icons/mdi/information";

export const AdminRoutes = (): CustomRouteObject[] => [
    {
        path: "admin",
        name: "Administration",
        element: <AdminPage />,
        children: [
            createIndexRoute("intro"),
            {
                path: "intro",
                name: "Introduction",
                icon: <InfoIcon />,
                element: <AdminIndexPage />,
                children: [],
            },
            ...EventScheduleEditorRoutes(),
            ...FAQEditorRoutes(),
            {
                path: "locations",
                name: "Locations",
                icon: <InfoIcon />,

                element: <LocationForm />,
            },
        ],
    },
];
