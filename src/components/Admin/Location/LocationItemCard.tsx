import { Card, CardContent, CardHeader, IconButton } from "@mui/material";
import { useBoolean } from "ahooks";
import { Remark } from "react-remark";
import { Marker } from "react-leaflet";
import { LatLng } from "leaflet";
import { useMutation } from "react-query";
import { useMemo } from "react";

import {
    backendKeys,
    deleteLocation,
    InformationLocation,
    InformationLocationWrite,
    updateLocation,
} from "../../../apis";
import { LeafletMap } from "../../Utilities/Leaflet/LeafletMap";
import { useMutationResponse } from "../../../hooks/useMutationResponse";

import { LocationEditForm } from "./LocationEditForm";

import EditIcon from "~icons/mdi/edit";
import CloseIcon from "~icons/mdi/close";

export type LocationItemCardProps = {
    location: InformationLocation;
};

export const LocationItemCard = ({ location }: LocationItemCardProps) => {
    const [editing, { toggle }] = useBoolean(false);
    const updateMutation = useMutation(
        updateLocation,
        useMutationResponse(
            "Successfully updated location",
            "Failed to update location",
            [backendKeys.locations(), backendKeys.publicInformation()]
        )
    );
    const deleteMutation = useMutation(
        (item: InformationLocationWrite) => deleteLocation(item),
        useMutationResponse(
            "Successfully deleted location",
            "Failed to delete location",
            [backendKeys.locations(), backendKeys.publicInformation()]
        )
    );

    const loc = useMemo(
        () => new LatLng(location.coordinate_lat, location.coordinate_lon),
        [location]
    );
    return (
        <Card>
            <CardHeader
                title={location.title}
                action={
                    <IconButton onClick={toggle}>
                        {editing ? <CloseIcon /> : <EditIcon />}
                    </IconButton>
                }
            />
            {editing ? (
                <LocationEditForm
                    location={location}
                    onSubmit={updateMutation.mutateAsync}
                    onDelete={deleteMutation.mutateAsync}
                />
            ) : (
                <CardContent>
                    <Remark>{location.description}</Remark>
                    <LeafletMap sx={{ height: 300 }} center={loc}>
                        <Marker position={loc} />
                    </LeafletMap>
                </CardContent>
            )}
        </Card>
    );
};
