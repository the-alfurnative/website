import { FormProvider, useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import {
    Button,
    ButtonGroup,
    CardActions,
    CardContent,
    Stack,
} from "@mui/material";

import {
    informationLocationOptions,
    InformationLocationWrite,
} from "../../../apis";
import {
    FormAutocomplete,
    FormCheckbox,
    FormMarkdownField,
    FormTextField,
} from "../../Utilities";
import { FormLocation } from "../../Utilities/RHF/FormLocation";

export type LocationEditFormProps = {
    location?: InformationLocationWrite;
    onSubmit: (data: InformationLocationWrite) => Promise<void>;
    onDelete?: (data: InformationLocationWrite) => Promise<void>;
};

export const LocationEditForm = ({
    location,
    onSubmit,
    onDelete,
}: LocationEditFormProps) => {
    const form = useForm<InformationLocationWrite>({
        defaultValues: location ?? {},
        resolver: zodResolver(InformationLocationWrite),
    });

    return (
        <FormProvider {...form}>
            <CardContent sx={{ maxHeight: "70vh", overflowY: "auto" }}>
                <Stack spacing={2}>
                    <FormTextField
                        name={"title"}
                        variant={"filled"}
                        label={"Location Title"}
                    />

                    <FormAutocomplete
                        options={informationLocationOptions}
                        name={"category"}
                        variant={"filled"}
                        label={"Category"}
                    />

                    <FormMarkdownField
                        name={"description"}
                        variant={"filled"}
                        label={"Description"}
                        helperText={
                            "A longer description for this item. Markdown is enabled."
                        }
                    />
                    <FormLocation
                        latField={"coordinate_lat"}
                        lonField={"coordinate_lon"}
                    />
                    <FormCheckbox
                        name={"public"}
                        label={"This location is available to the public"}
                    />
                </Stack>
            </CardContent>
            <CardActions>
                <ButtonGroup fullWidth>
                    {onDelete && (
                        <Button
                            color={"error"}
                            onClick={() => {
                                if (location && onDelete) {
                                    onDelete(location);
                                }
                            }}
                        >
                            Delete
                        </Button>
                    )}
                    <Button
                        title={JSON.stringify(form.formState.errors)}
                        onClick={form.handleSubmit((data) =>
                            onSubmit(data).then(() => form.reset({}))
                        )}
                    >
                        Save
                    </Button>
                </ButtonGroup>
            </CardActions>
        </FormProvider>
    );
};
