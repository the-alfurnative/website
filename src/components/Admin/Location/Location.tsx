import { useQuery } from "react-query";

import { Brick, Masonry } from "../../Layout";
import { backendKeys, getLocations } from "../../../apis";
import { unwrapAxios } from "../../../utilities";

import { LocationCreateCard } from "./LocationCreateCard";
import { LocationItemCard } from "./LocationItemCard";

export const LocationForm = () => {
    const { data = [] } = useQuery(backendKeys.locations(), () =>
        getLocations().then(unwrapAxios)
    );
    return (
        <Masonry sx={{ p: 2 }}>
            <Brick>
                <LocationCreateCard />
            </Brick>
            {data.map((it) => (
                <Brick key={it.id}>
                    <LocationItemCard location={it} />
                </Brick>
            ))}
        </Masonry>
    );
};
