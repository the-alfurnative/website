import { Card, CardHeader } from "@mui/material";
import { useMutation } from "react-query";

import {
    createLocation,
    backendKeys,
    InformationLocationWrite,
} from "../../../apis";
import { useMutationResponse } from "../../../hooks/useMutationResponse";

import { LocationEditForm } from "./LocationEditForm";

export const LocationCreateCard = () => {
    const createMutation = useMutation(
        (data: InformationLocationWrite) => createLocation(data),
        useMutationResponse(
            "Created a new location item",
            "Failed to create a new location item",
            [backendKeys.locations(), backendKeys.publicInformation()]
        )
    );
    return (
        <Card>
            <CardHeader title={"Create new location"} />

            <LocationEditForm onSubmit={createMutation.mutateAsync} />
        </Card>
    );
};
