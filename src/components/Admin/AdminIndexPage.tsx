import { Box, Chip, Container, Typography } from "@mui/material";

import { useRoles } from "../Authentication/useRoles";

export const AdminIndexPage = () => {
    const roles = useRoles();
    return (
        <Container maxWidth={"md"}>
            <Typography variant={"h2"} gutterBottom>
                Alfurnative Staff System
            </Typography>

            <Typography gutterBottom>
                With this system you can access the parts of the convention you
                need to do your job. Information in this system is sensitive, so
                please do not show this around
            </Typography>

            <Typography gutterBottom>You have to following roles</Typography>

            <Box display={"flex"} flexDirection={"row"} gap={2}>
                {roles?.map((it) => (
                    <Chip label={it} key={it} />
                ))}
            </Box>
        </Container>
    );
};
