# THE CURRENT STATE AND FUTURE OF ALFURNATIVE

### What happened to AFn?
Since the first edition was organized, communication between me, the chairman, and staff was not optimal. We had various meetings to discuss this issue to try and avoid it happening again. I took too much on myself, and in turn made the staff not do enough to keep proper track of what was being done, how and when.
Despite my best efforts to avoid this issue in the past edition, I fell back into the same habit. I was doing too much on my own, and not communicating properly what I was doing at the time. When I started getting health issues, staff received even less information due to me being in and out of the hospital, which is an understandable consequence.
Due to sickness and the overwhelming situation, I have not been able to keep adequate watch over finances, of which I have been the only one in charge of and control over. Things were more expensive than expected, and instead of putting a halt to it and communicating, I made the decision to try and keep my head high to not make us worry. In the end, this turned out to be the wrong decision.
Alfurnative is now considered completely bankrupt. There is no money left and even I myself am in the red to try and still make everything possible out of my own pocket.

### What happened to the merch and what will happen?
Merchandise was ordered through a company in China, to try and get a cheaper option due to the other already more expensive expenses made. However, this turned out to be a wrong decision as well, as this company did likely not put sufficient information on the package for customs. After it got sent out, it did not even get past Chinese customs, where it was held. We are assuming the merchandise got destroyed.
Despite my best efforts, I was unable to acquire any form of compensation for this. I have gone down all suggested and advised routes, but didn't get anywhere.
I am very sad to say that we cannot issue any refunds, nor get you the merchandise you paid for. Due to Alfurnative being in the red, we do not have any money to refund you.

### The future of this con
Due to bankruptcy, unpaid invoices from outside companies involved and the loss of merch, there are zero grounds to build on. Staff has made the tough decision to not continue the convention, with or without me. There will be no further events holding the Alfurnative name.

### In the end, what does this all mean?
Unfortunately, The Alfurnative as a convention, is done. There will be no next edition, nor any other event under this name. The staff team will disband and go their own ways.
I am extremely sorry to bring you this bad news, and completely understand you might feel angry or upset. You are in your right to. I do want to ask you, however, to understand that our staff team was not in control over anything involving this situation besides looking for absolutely every possible option to fix the damage done. They came up emptyhanded.

-Chairman of Alfurnative, Hunter