import { describe, expect, it } from "vitest";

import {
    getRegistrationState,
    regClosedDay,
    regStartsDay,
} from "./useRegistationState";

describe("useRegistrationState", function () {
    describe("getRegistrationState", function () {
        it.each([
            { date: regClosedDay.add(1, "minute"), expected: "concluded" },
            { date: regClosedDay.subtract(1, "minute"), expected: "open" },
            { date: regStartsDay.add(1, "minute"), expected: "open" },
            {
                date: regStartsDay.subtract(1, "minute"),
                expected: "starting_soon",
            },
            {
                date: regStartsDay.subtract(1, "hour"),
                expected: "starting_today",
            },
            {
                date: regStartsDay.startOf("day"),
                expected: "starting_today",
            },
            {
                date: regStartsDay.subtract(1, "day"),
                expected: "closed",
            },
        ])("returns $result after for $date", ({ date, expected }) => {
            const result = getRegistrationState(date);

            expect(result).to.equal(expected);
        });
    });
});
