import { UseMutationOptions, useQueryClient } from "react-query";
import { useSnackbar } from "notistack";
import { useMemo } from "react";
import { AxiosError } from "axios";
import { nanoid } from "nanoid";

export const useMutationResponse = (
    successMessage: string,
    errorMessage: string,
    filterKeys: string[][] = []
): Omit<
    UseMutationOptions<any, AxiosError, any, any>,
    "mutationFn" | "mutationKeys"
> => {
    const queryClient = useQueryClient();
    const { enqueueSnackbar } = useSnackbar();

    return useMemo(
        (): Omit<
            UseMutationOptions<any, AxiosError, any, any>,
            "mutationFn" | "mutationKeys"
        > => ({
            onSuccess: async () => {
                enqueueSnackbar(successMessage, {
                    variant: "success",
                    autoHideDuration: 2000,
                    key: nanoid(),
                });

                return Promise.all(
                    filterKeys.map((it) => queryClient.invalidateQueries(it))
                );
            },
            onError: (err) => {
                enqueueSnackbar(
                    `${errorMessage} (Status Code ${err.response?.status})`,
                    {
                        variant: "error",
                        autoHideDuration: 10000,
                        key: nanoid(),
                    }
                );
                return Promise.all(
                    filterKeys.map((it) => queryClient.invalidateQueries(it))
                );
            },
        }),
        [queryClient, enqueueSnackbar]
    );
};
