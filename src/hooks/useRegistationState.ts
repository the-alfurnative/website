import dayjs, { Dayjs } from "dayjs";
import { match, P } from "ts-pattern";
import { useState } from "react";
import { useInterval } from "ahooks";

export const regStartsDay = dayjs("2023-04-29T12:00:00.000+02:00");
export const regClosedDay = dayjs("2023-09-17T23:59:59.999+02:00");
export type RegistrationState =
    | "open"
    | "starting_today"
    | "starting_soon"
    | "closed"
    | "concluded";
export const getRegistrationState = (item: Dayjs = dayjs()) =>
    match(item)
        .returnType<RegistrationState>()
        .with(
            P.when((it) => it.isBetween(regStartsDay, regClosedDay)),
            () => "open"
        )
        .with(
            P.when((it) => it.isAfter(regClosedDay)),
            () => "concluded"
        )
        .with(
            P.when((it) =>
                it.isBetween(regStartsDay.subtract(1, "hour"), regStartsDay)
            ),
            () => "starting_soon"
        )
        .with(
            P.when((it) => it.isSame(regStartsDay, "day")),
            () => "starting_today"
        )
        .with(
            P.when((it) => it.isSameOrBefore(regStartsDay)),
            () => "closed"
        )
        .otherwise(() => "closed");

export const useRegistrationState = (
    refreshInterval = 1000
): RegistrationState => {
    const [state, setState] = useState<RegistrationState>(getRegistrationState);

    useInterval(() => {
        setState(getRegistrationState());
    }, refreshInterval);

    return state;
};
