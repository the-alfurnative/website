import { useScroll } from "ahooks";
import { useMemo } from "react";
import { useLocation } from "react-router-dom";

export const useHasScrolled = () => {
    const location = useLocation();
    const pos = useScroll(document);

    return useMemo(() => {
        if (location.pathname === "/") {
            if (pos !== undefined) {
                return pos.top > 0;
            }
            return true;
        }
        return true;
    }, [pos]);
};
