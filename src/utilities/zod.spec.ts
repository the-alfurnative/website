import { describe, it, expect } from "vitest";
import dayjs from "dayjs";

import { zDayjs, zDayjsString } from "./zod";

describe("zod", function () {
    describe("zDayJs", function () {
        it("can parse a dayjs object", () => {
            const value = dayjs();

            const result = zDayjs.parse(value);
            expect(result).to.eq(value);
        });

        it("cannot parse an ISO formatted string", () => {
            const value = dayjs().toISOString();

            const result = zDayjs.safeParse(value);
            expect(result.success).to.false;
        });
    });
    describe("zDayjsString", function () {
        it("can parse a dayjs object", () => {
            const value = dayjs();

            const result = zDayjsString.parse(value);
            expect(result).to.eq(value.toISOString());
        });

        it("can parse an ISO formatted string", () => {
            const value = dayjs().toISOString();

            const result = zDayjsString.parse(value);
            expect(result).to.eq(value);
        });
    });
});
