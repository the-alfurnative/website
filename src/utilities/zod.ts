import z, { ZodString } from "zod";
import { Dayjs, isDayjs } from "dayjs";

export const zDayjs = z.custom<Dayjs>((it) => isDayjs(it));

export const zDayjsString = z
    .union([
        zDayjs
            .transform((it: Dayjs) => it.toISOString())
            .pipe(z.string().datetime({ offset: true })),
        z.string().datetime({ offset: true }),
    ])
    .pipe(z.string().datetime({ offset: true }));

export const zStringOptional = (item: ZodString) =>
    z.union([
        item,
        z
            .literal("")
            .transform(() => null)
            .pipe(z.null()),
    ]);

export const zTranslatedString = z.record(z.string().min(5)).default({});
