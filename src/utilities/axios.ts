import type { AxiosPromise, AxiosResponse } from "axios";

declare global {
    // eslint-disable-next-line no-var
    var FETCH_TOKEN: string | undefined;
}

export const setFetchToken = (token: string | undefined) => {
    globalThis.FETCH_TOKEN = token;
};

export const sleep = async (amount: number) =>
    new Promise((resolve) => setTimeout(resolve, amount));

export const unpackedAxios = (fn: () => AxiosPromise) =>
    fn().then((r) => r.data);

export const unwrapAxios = <DataType>(
    response: AxiosResponse<DataType>
): DataType => response.data;

export const waitForFetchToken = async () => {
    while (globalThis.FETCH_TOKEN === undefined) {
        await sleep(10);
    }

    return globalThis.FETCH_TOKEN;
};
