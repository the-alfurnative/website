import { chain } from "lodash-es";

import { publicBackendClient } from "./client";
import {
    PublicEventScheduleItem,
    PublicLocation,
    PublicQuestion,
} from "./models";

export type PublicInformation = {
    questions: PublicQuestion[];
    locations: PublicLocation[];
};

export const getPublicInformation = () =>
    publicBackendClient.get<PublicInformation>("/information/info").then(
        (data): PublicInformation => ({
            questions: chain(data.data.questions)
                .orderBy(
                    [(it) => it.category, (it) => it.question],
                    ["asc", "asc"]
                )
                .value(),
            locations: data.data.locations,
        })
    );
export const getPublicSchedule = () =>
    publicBackendClient
        .get<PublicEventScheduleItem[]>("/schedule/")
        .then((res) =>
            res.data
                .map((it) => {
                    const parsed = PublicEventScheduleItem.safeParse(it);

                    if (parsed.success) {
                        return parsed.data;
                    }
                    return undefined;
                })
                .filter((it): it is PublicEventScheduleItem => it !== undefined)
        );
