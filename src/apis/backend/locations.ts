import { backendClient } from "./client";
import { InformationLocation, InformationLocationWrite } from "./models";

export const getLocations = () =>
    backendClient.get<InformationLocation[]>("/information/locations/");

export const createLocation = (data: InformationLocationWrite) =>
    backendClient.post<InformationLocation>("/information/locations/", data);

export const updateLocation = ({ id, ...data }: InformationLocationWrite) =>
    backendClient.put<InformationLocation>(
        `/information/locations/${id}`,
        data
    );

export const deleteLocation = ({
    id,
}: InformationLocation | InformationLocationWrite) =>
    backendClient.delete<InformationLocation>(`/information/locations/${id}`);
