import axios from "axios";

import { waitForFetchToken } from "../../utilities";

export const backendClient = axios.create({
    baseURL: import.meta.env.DEV
        ? "https://afn-backend.requinard.nl/"
        : "https://afn-backend.requinard.nl/",
});

backendClient.interceptors.request.use(async (value) => {
    const token = await waitForFetchToken();

    value.headers.setAuthorization(`Bearer ${token}`);
    return value;
});

export const publicBackendClient = axios.create({
    baseURL: backendClient.defaults.baseURL,
});
