import z from "zod";

import { zTranslatedString } from "../../../utilities";

import { BackendBaseReadable } from "./BackendBaseReadable";
import { BackendAdminWritable } from "./BackendAdminWritable";

export const ScheduleTrackWrite = BackendBaseReadable.extend({
    name: z.string().min(3),
    name_translations: zTranslatedString,
}).and(BackendAdminWritable);
export type ScheduleTrackWrite = z.infer<typeof ScheduleTrackWrite>;
