import { z } from "zod";

import { BackendBaseReadable } from "./BackendBaseReadable";

export const FAQQuestionWrite = BackendBaseReadable.extend({
    public: z.boolean().default(false),
    question: z.string().min(10),
    answer: z.string().min(10),
    category: z.string().min(5),
});
export type FAQQuestionWrite = z.infer<typeof FAQQuestionWrite>;
