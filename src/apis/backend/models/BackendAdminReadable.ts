import z from "zod";

export const BackendAdminReadable = z.object({
    internal_name: z.string(),
    date_created: z.string().datetime({ offset: true }),
    date_edited: z.string().datetime({ offset: true }),
});

export type BackendAdminReadable = z.infer<typeof BackendAdminReadable>;
