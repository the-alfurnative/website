import z from "zod";

export const BackendBaseReadable = z.object({
    id: z
        .string()
        .uuid()
        .default(() => crypto.randomUUID()),
    order: z.number().positive().default(100),
});
export type BackendBaseReadable = z.infer<typeof BackendBaseReadable>;
