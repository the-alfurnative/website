import { z } from "zod";

import { BackendBaseReadable } from "./BackendBaseReadable";

export const informationLocationOptions = [
    "location",
    "hotel",
    "restaurant",
    "bar",
    "parking",
] as const;
export const InformationLocationWrite = BackendBaseReadable.extend({
    coordinate_lat: z.number(),
    coordinate_lon: z.number(),
    title: z.string(),
    description: z.string(),
    category: z.enum(informationLocationOptions).default("location"),
    public: z.boolean().default(false),
});

export type InformationLocationWrite = z.infer<typeof InformationLocationWrite>;
