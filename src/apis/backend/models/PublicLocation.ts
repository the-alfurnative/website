import z from "zod";

import { BackendBaseReadable } from "./BackendBaseReadable";
import { informationLocationOptions } from "./InformationLocationWrite";

export const PublicLocation = BackendBaseReadable.extend({
    title: z.string(),
    category: z.enum(informationLocationOptions).default("location"),
    description: z.string(),
    coordinate_lat: z.number(),
    coordinate_lon: z.number(),
});
export type PublicLocation = z.infer<typeof PublicLocation>;
