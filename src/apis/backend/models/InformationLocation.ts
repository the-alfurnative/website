import { z } from "zod";

import { InformationLocationWrite } from "./InformationLocationWrite";
import { BackendAdminReadable } from "./BackendAdminReadable";

export const InformationLocation =
    InformationLocationWrite.and(BackendAdminReadable);
export type InformationLocation = z.infer<typeof InformationLocation>;
