import { z } from "zod";

import { FAQQuestionWrite } from "./FAQQuestionWrite";
import { BackendAdminReadable } from "./BackendAdminReadable";

export const FAQQuestion = FAQQuestionWrite.and(BackendAdminReadable);
export type FAQQuestion = z.infer<typeof FAQQuestion>;
