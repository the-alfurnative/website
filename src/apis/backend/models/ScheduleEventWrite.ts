import z from "zod";
import dayjs from "dayjs";

import {
    zDayjsString,
    zStringOptional,
    zTranslatedString,
} from "../../../utilities";

import { BackendBaseReadable } from "./BackendBaseReadable";
import { BackendAdminWritable } from "./BackendAdminWritable";

export const ScheduleEventWrite = BackendBaseReadable.extend({
    title: z.string().min(5),
    title_translations: zTranslatedString,
    subtitle: zStringOptional(z.string().min(5)),
    subtitle_translations: zTranslatedString,
    short_description: zStringOptional(z.string().min(5).max(180)),
    short_description_translations: zTranslatedString,
    description: z.string().min(10),
    description_translations: zTranslatedString,
    hosts: z.string().min(3).array().default([]),
    room_uuid: z.string().uuid().nullish(),
    track_uuid: z.string().uuid().nullish(),
    requirements: z.string().min(3).array(),
    event_start: zDayjsString.nullish(),
    event_end: zDayjsString.nullish(),
    flags: z.string().min(1).array().default([]),
})
    .refine(
        (it) =>
            it.event_start && it.event_end
                ? dayjs(it.event_end).isAfter(it.event_start)
                : true,
        {
            path: ["event_start"],
            message: "An event cannot start after is has already ended.",
        }
    )
    .and(BackendAdminWritable);

export type ScheduleEventWrite = z.infer<typeof ScheduleEventWrite>;
