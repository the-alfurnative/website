import z from "zod";

import { ScheduleEventWrite } from "./ScheduleEventWrite";
import { BackendAdminReadable } from "./BackendAdminReadable";

export const ScheduleEvent = ScheduleEventWrite.and(
    z.object({
        published_event_id: z.string().uuid().nullish(),
    })
).and(BackendAdminReadable);
export type ScheduleEvent = z.infer<typeof ScheduleEvent>;
