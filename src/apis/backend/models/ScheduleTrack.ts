import z from "zod";

import { ScheduleTrackWrite } from "./ScheduleTrackWrite";
import { BackendAdminReadable } from "./BackendAdminReadable";

export const ScheduleTrack = ScheduleTrackWrite.and(BackendAdminReadable);
export type ScheduleTrack = z.infer<typeof ScheduleTrack>;
