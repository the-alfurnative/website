import z from "zod";

export const BackendAdminWritable = z.object({
    order: z.number().positive().default(100),
    notes: z.string().nullish(),
});
export type BackendAdminWritable = z.infer<typeof BackendAdminWritable>;
