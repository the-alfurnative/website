import z from "zod";

import { zTranslatedString } from "../../../utilities";

import { BackendBaseReadable } from "./BackendBaseReadable";

export const PublicEventScheduleItem = BackendBaseReadable.extend({
    title: z.string(),
    title_translations: zTranslatedString,
    subtitle: z.string().optional(),
    description: z.string(),
    description_translations: zTranslatedString,
    short_description: z.string().optional(),
    short_description_translations: zTranslatedString,
    hosts: z.string().array(),
    room: z.string(),
    track: z.string(),
    flags: z.string().array(),
    event_start: z.string().datetime({ offset: true }),
    event_end: z.string().datetime({ offset: true }),
});
export type PublicEventScheduleItem = z.infer<typeof PublicEventScheduleItem>;
