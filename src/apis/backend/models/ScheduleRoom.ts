import z from "zod";

import { BackendAdminReadable } from "./BackendAdminReadable";
import { ScheduleRoomWrite } from "./ScheduleRoomWrite";

export const ScheduleRoom = BackendAdminReadable.and(ScheduleRoomWrite);
export type ScheduleRoom = z.infer<typeof ScheduleRoom>;
