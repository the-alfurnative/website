import z from "zod";

import { BackendBaseReadable } from "./BackendBaseReadable";

export const PublicQuestion = BackendBaseReadable.extend({
    id: z.string().uuid(),
    question: z.string(),
    answer: z.string(),
    category: z.string(),
});

export type PublicQuestion = z.infer<typeof PublicQuestion>;
