import { backendClient } from "./client";
import { FAQQuestionWrite, FAQQuestion } from "./models";

export const createNewFAQItem = (faq: FAQQuestionWrite) =>
    backendClient.post<FAQQuestionWrite, FAQQuestion>(
        "information/questions/",
        faq
    );

export const deleteFAQItem = (faq: FAQQuestionWrite) =>
    backendClient.delete(`information/questions/${faq.id}`);
export const getFAQItems = () =>
    backendClient.get<FAQQuestion[]>("information/questions/");
export const updateFAQItem = (faq: FAQQuestionWrite) =>
    backendClient.put(`information/questions/${faq.id}`, faq);
