import { backendClient } from "./client";
import {
    ScheduleEvent,
    ScheduleEventWrite,
    ScheduleRoom,
    ScheduleRoomWrite,
    ScheduleTrack,
    ScheduleTrackWrite,
} from "./models";

export const createNewEvent = () =>
    backendClient.post("/schedule/events/", {
        id: crypto.randomUUID(),
        order: 100,
        title: "New Event",
        description: "A new event",
    });

export const createScheduleRoom = () =>
    backendClient.post("/schedule/rooms/", {
        id: crypto.randomUUID(),
        order: 100,
        name: "New Room",
        name_translations: {},
        description: "Empty Description",
    } as ScheduleRoomWrite);

export const createScheduleTrack = () =>
    backendClient.post("/schedule/tracks/", {
        id: crypto.randomUUID(),
        order: 100,
        name: "New Track",
        notes: "",
        name_translations: {},
        description: "Empty Description",
    } as ScheduleTrackWrite);

export const deleteEvent = (id: string) =>
    backendClient.delete(`/schedule/events/${id}`);

export const deleteScheduleRoom = (room: ScheduleRoom) =>
    backendClient.delete(`/schedule/rooms/${room.id}`);
export const deleteScheduleTrack = (track: ScheduleTrack) =>
    backendClient.delete(`/schedule/tracks/${track.id}`);

export const getScheduleEvents = () =>
    backendClient.get<ScheduleEvent[]>("/schedule/events/");

export const getScheduleRooms = () =>
    backendClient.get<ScheduleRoom[]>("/schedule/rooms/");

export const getScheduleTracks = () =>
    backendClient.get<ScheduleTrack[]>("/schedule/tracks/");
export const publishEvent = (id: string) =>
    backendClient.post(`/schedule/events/${id}/publish`);

export const unpublishEvent = (id: string) =>
    backendClient.delete(`/schedule/events/${id}/publish`);

export const updateEvent = ({ id, ...item }: ScheduleEventWrite) =>
    backendClient.put<ScheduleEvent>(`/schedule/events/${id}`, item);
export const updateScheduleRoom = ({ id, ...item }: ScheduleRoomWrite) =>
    backendClient.put<ScheduleRoom>(`/schedule/rooms/${id}`, item);

export const updateScheduleTrack = ({ id, ...item }: ScheduleTrackWrite) =>
    backendClient.put<ScheduleTrack>(`/schedule/tracks/${id}`, item);
