export * from "./client";
export * from "./locations";
export * from "./models";
export * from "./public";
export * from "./schedule";
export const backendKeys = {
    base: ["backend"] as const,
    publicSchedule: () => [...backendKeys.base, "public", "schedule"],

    baseSchedule: () => [...backendKeys.base, "schedule"],
    scheduleEvents: () => [...backendKeys.baseSchedule(), "schedule"],
    scheduleRooms: () => [...backendKeys.baseSchedule(), "rooms"],
    scheduleTracks: () => [...backendKeys.baseSchedule(), "tracks"],
    baseQuestions: () => [...backendKeys.base, "questions"],
    questionsFAQ: () => [...backendKeys.baseQuestions(), "faq"],

    baseInformation: () => [...backendKeys.base, "information"],
    locations: () => [...backendKeys.baseInformation(), "locations"],
    publicInformation: () => [...backendKeys.base, "public", "information"],
} as const;
